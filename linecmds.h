void cmd_delete(size_t argc, char** argv, char *cmd, void *v);
void cmd_rename(size_t argc, char** argv, char *cmd, void *v);
void cmd_regex_rename(size_t argc, char** argv, char *cmd, void *v);
void cmd_touch(size_t argc, char** argv, char *cmd, void *v);
void cmd_mkdir(size_t argc, char** argv, char *cmd, void *v);
void cmd_cd(size_t argc, char** argv, char *cmd, void *v);
void cmd_regsel(size_t argc, char** argv, char *cmd, void *v);
void cmd_clearsel(size_t argc, char** argv, char *cmd, void *v);
void cmd_writesel(size_t argc, char** argv, char *cmd, void *v);
void cmd_open_with(size_t argc, char** argv, char *cmd, void *v);
