#include "all.h"

static mark *tmrks = 0;
size_t s_tmrks = 0;

char *
getmark(Rune r)
{
    size_t i;
    for(i = 0; i < s_tmrks; i++)
        if(tmrks[i].r == r)
            return tmrks[i].path;
    for(i = 0; i < s_bmrks; i++)
        if(bmrks[i].r == r)
            return bmrks[i].path;
    return 0;
}
void
setmark(Rune r, char *path)
{
    size_t i;
    for(i = 0; i < s_tmrks; i++) {
        if(tmrks[i].r == r) {
            free(tmrks[i].path);
            tmrks[i].path = estrdup(path);
            return;
        }
    }
    tmrks = ereallocarray(tmrks, ++s_tmrks, sizeof(mark));
    tmrks[s_tmrks - 1].r = r;
    tmrks[s_tmrks - 1].path = estrdup(path);
}
