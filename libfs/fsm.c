#include "../all.h"

/*if stat fails getfileinfo will set st = st_t and return 0
 * this happens we are likely stating a symlink which has a target
 * witch a parent directory we do not habe execute permissions for
 * this may result in a symlink that points to a direcory beeing sorted
 * as a file, but it definietly beats not showing it at all.*/
int 
getfileinfo(char *file, fileinfo *fi)
{
        if (lstat(file, &fi->st))
            return 1;
        if (stat(file, &fi->st_t))
            fi->st_t = fi->st; 
        fi->csize = -1;
        fi->name = estrdup(file);
        return 0;
}
int
getdirlist(char *dir, fileinfo **dl, size_t *dl_size)
{
    DIR *dp;
    struct dirent *d;
    size_t msize = 16;
    if (chdir(dir) < 0)
        return 1;
    if (!(dp = opendir(dir)))
        return 1;
    *dl_size = 0;
    *dl = ereallocarray(NULL, msize, sizeof(fileinfo));
    while ((d = readdir(dp))) {
        if (!strcmp(d->d_name, ".") || !strcmp(d->d_name, ".."))
            continue;
        if (msize < ++(*dl_size))
            *dl = ereallocarray(*dl, (msize = msize << 1), sizeof(fileinfo));
        if (getfileinfo(d->d_name, &((*dl)[*dl_size - 1]))) {
            for (size_t i = 0; i < *dl_size - 1; i++)
                free((*dl)[i].name);
            closedir(dp);
            free(*dl), *dl = NULL, *dl_size = 0;
            return 1;
        }
    }
    closedir(dp);
    if (*dl_size)
        *dl = ereallocarray(*dl, *dl_size, sizeof(fileinfo));
    else
        free(*dl), *dl = NULL;
    return 0;
}
