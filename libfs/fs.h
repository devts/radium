typedef struct fileinfo fileinfo;
struct fileinfo
{
    char* name;
    off_t csize;
    struct stat st;    /*stat of name istself/ lstat*/
    struct stat st_t;  /*stat of link target / stat*/
};
int getfileinfo(char *file, fileinfo *fi);
int getdirlist(char *dir, fileinfo **dl, size_t *dl_size);

enum sort_type
{
    SORT_NATURAL,
    SORT_BASENAME,
    SORT_MTIME,
    SORT_CTIME,
    SORT_ATIME,
    SORT_SIZE,
};
typedef struct
{
    int type; /*a sort_type SORT_ value*/
    int icase;
    int reverse;
    int dirs_first;
    
} sort;
void fsort(sort s, fileinfo* files, size_t len);

char *canonic_path(const char *dir, const char *arg);

mode_t getumask(void);
mode_t parsemode(const char *, mode_t, mode_t);

/* The functions provided in fsutil.c are not general purpose
 * commands, but rather designed for specific usage by radium.*/

/* fs_cp will copy the contents of src to dst/basename(src)
 * if the name exits underscores will be appended
 * it shall never overwrite existing files or directories
 * fs_cp will always copy recursivly and never follow symlinks
 * it will attemt to preserve atime, mtime, premissions and owner
 * failure to change the owner is not checked and will not be 
 * reported as error.
 * if fs_cp succedes the return value shall be 0*/
int fs_cp(const char *src, const char *dst);
/* fs_rm will recursivly delete the contents of src
 * it will even delete across fs boundaries*/
int fs_rm(const char *src);
/* like cp, will try to rename (3), on EXDEV it calls cp
 * and if the cp surccedes it will call rm*/
int fs_mv(const char *src, const char *dst);
/* rename(3) src to dst, appending _ if dst exists*/
int fs_rename(const char *src, const char *dst);
/* fs_mkdir will create a dir named path.
 * is is not recursive, the existence of dirname(path) is requirend*/
int fs_mkdir(const char* path);
/* fs_touch will create the file if it does not exist and update the
 * atime and mtime to the current time if it does.*/
int fs_touch(const char *file);
/* fs_chmod will attempt to change the mode of path
 * according to the given modestr. It is non-recursive.*/
int fs_chmod(const char *modestr, const char *path);
int fs_csize(const char *src, off_t *s);
