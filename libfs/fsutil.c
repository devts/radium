#include "../all.h"

static int
recurse(const char *s1, const char *s2, int dirsfirst, int (*fn)(const char *s1, struct stat *st, const char *s2, size_t depth, void *arg), size_t depth, void *arg)
{
	DIR *dp;
	struct dirent *d;
	struct stat st;
	char *ns1, *ns2;
    int r_status = 0;

	if (lstat(s1, &st) < 0) {
		weprintf("lstat: %s:", s1);
		return 1;
	}

	if (S_ISDIR(st.st_mode)) {
		if (!(dp = opendir(s1))) {
			weprintf("opendir %s:", s1);
			return 1;
		}
        if(dirsfirst)
            r_status |= fn(s1, &st, s2, depth, arg);
        if(r_status)
            return r_status;

		while ((d = readdir(dp))) {
			if (!strcmp(d->d_name, ".") || !strcmp(d->d_name, ".."))
				continue;

            ns1 = pathmcat(s1, d->d_name);
            if (s2)
                ns2 = pathmcat(s2, d->d_name);

			r_status |= recurse(ns1, s2 ? ns2 : NULL, dirsfirst, fn, depth + 1, arg);

            free(ns1);
            if (s2)
                free(ns2);
		}
		closedir(dp);
    }
    r_status |= fn(s1, &st, s2, depth, arg);
	return r_status;
}
static int
concat(FILE *fp1, FILE *fp2)
{
	char buf[4096];
	size_t n;
	while ((n = fread(buf, 1, sizeof(buf), fp1))) {
		fwrite(buf, 1, n, fp2);

		if (ferror(fp1) || ferror(fp2))
			return 1;
		if (feof(fp1))
			return 0;
	}
    if (ferror(fp1) || ferror(fp2))
        return 1;
    if (feof(fp1))
        return 0;
    return 1;
}
static int
cp_fn(const char *s1, struct stat *st, const char *s2, size_t depth, void *arg)
{
    if(!st || !arg)
        return 1;
    struct stat *rdst = (struct stat *)arg;
	if (S_ISDIR(st->st_mode)) {
        struct stat stb;
		if (s2 && mkdir(s2, st->st_mode) < 0 && (errno != EEXIST || lstat(s2, &stb) || !S_ISDIR(stb.st_mode))) {
			weprintf("mkdir %s:", s2);
			return 1;
		}
        if (!depth) {
            if(lstat(s2, rdst))
                return 1;
        } else if ( st->st_dev == rdst->st_dev
            && st->st_ino == rdst->st_ino) {
            weprintf("%s -> %s: recursive operation\n", s1, s2);
            return 1;
        }
    } else if (S_ISLNK(st->st_mode)) {
        char target[PATH_MAX];
	    ssize_t r;
		if ((r = readlink(s1, target, sizeof(target) - 1)) >= 0) {
			target[r] = '\0';
			if (symlink(target, s2) < 0) {
				weprintf("symlink %s -> %s:", s2, target);
				return 1;
			}
		} else {
        return 1;
        }
	} else if (S_ISBLK(st->st_mode) || S_ISCHR(st->st_mode) || S_ISSOCK(st->st_mode) || S_ISFIFO(st->st_mode)) {
		if (mknod(s2, st->st_mode, st->st_rdev) < 0) {
			weprintf("mknod %s:", s2);
			return 1;
		}
	} else {
        struct stat sta, stb;
        if (!lstat(s1, &sta)
            && !lstat(s2, &stb)
            && sta.st_dev == stb.st_dev
            && sta.st_ino == stb.st_ino) {
            weprintf("%s -> %s: same file\n", s1, s2);
            return 1;
        }
	    FILE *f1, *f2;
		if (!(f1 = fopen(s1, "r"))) {
			weprintf("fopen %s:", s1);
			return 1;
		}
		if (!(f2 = fopen(s2, "w"))) {
            weprintf("fopen %s:", s2);
            return 1;
		}
		if(concat(f1, f2)) {
            weprintf("error while reading file");
            return 1;
        }

		if (fclose(f2)) {
			weprintf("fclose %s:", s2);
			return 1;
		}
		if (fclose(f1)) {
			weprintf("fclose %s:", s1);
			return 1;
		}
	}
    if (!S_ISLNK(st->st_mode)) {
        if(chmod(s2, st->st_mode)){
            weprintf("chmod %s:", s2);
            return 1;
        }
	    struct timespec times[2];
        times[0] = st->st_atim;
        times[1] = st->st_mtim;
        utimensat(AT_FDCWD, s2, times, 0);
    }
    lchown(s2, st->st_uid, st->st_gid);
    return 0;
}
static int
rm_fn(const char *s1, struct stat *st, const char *s2, size_t depth, void *arg)
{
	if (st && S_ISDIR(st->st_mode)) {
		if (rmdir(s1) < 0 && errno != ENOENT) {
            weprintf("rmdir %s:", s1);
            return 1;
		}
	} else if (unlink(s1) < 0 && errno != ENOENT) {
        weprintf("unlink %s:", s1);
        return 1;
	}
    return 0;
}
int
fs_rm(const char *src)
{
    return recurse(src, NULL, 0, (rm_fn), 0, 0);
}
static int
cp_mv(const char *src, const char *dst, int mv)
{
	struct stat sta, stb, rdst;
    if(lstat(src, &sta) || lstat(dst, &stb) || !S_ISDIR(stb.st_mode))
        return 1;
    char *bname = estrdup(src);
    char *buf = pathmcat(dst, basename(bname));
    free(bname);
    while (!lstat(buf, &stb)) {
        char *tmp = strmcat(buf, "_");
        free(buf);
        buf = tmp;
    }
    if(mv) {
        if (!rename(src, buf)) {
            free(buf);
            return 0;
        }
        if (errno == EXDEV) {
            if (recurse(src, buf, 1, (cp_fn), 0, &rdst)) {
                free(buf);
                return 1;
            }
            free(buf);
            return fs_rm(src);
        }
        free(buf);
        return 1;
    } else {
        int r = recurse(src, buf, 1, (cp_fn), 0, &rdst);
        free(buf);
        return r;
    }
}
int
fs_cp(const char *src, const char *dst)
{
    return cp_mv(src, dst, 0);
}
int
fs_mv(const char *src, const char *dst)
{
    return cp_mv(src, dst, 1);
}
int
fs_rename(const char *src, const char *dst)
{
	struct stat sta, stb;
    if(lstat(src, &sta))
        return 1;
    char *buf = estrdup(dst);
    size_t dlen = strlen(buf);
    if(buf[dlen - 1] == '/')
        buf[dlen - 1] = '\0';
    while (!lstat(buf, &stb)) {
        char *tmp = strmcat(buf, "_");
        free(buf);
        buf = tmp;
    }
    int r = rename(src, buf);
    free(buf);
    return r;
}
int
fs_mkdir(const char* path)
{
    struct stat st;
	if (mkdir(path, S_IRWXU | S_IRWXG | S_IRWXO) < 0 && (errno != EEXIST || lstat(path, &st) || !S_ISDIR(st.st_mode)) ) {
		weprintf("mkdir %s:", path);
		return 1;
	}
    return 0;
}
int
fs_touch(const char *file)
{
	int fd;
	struct stat st;

	if (stat(file, &st)) {
		if (errno != ENOENT) {
			weprintf("stat %s:", file);
            return 1;
        }
        if ((fd = open(file, O_CREAT | O_EXCL, 0644)) < 0) {
            weprintf("open %s:", file);
            return 1;
        }
        close(fd);
	} else {
        static struct timespec times[2];
        clock_gettime(CLOCK_REALTIME, &times[0]);
        times[1] = times[0];
		if (utimensat(AT_FDCWD, file, times, 0) < 0) {
			weprintf("utimensat %s:", file);
		    return 1;
        }
    }
    return 0;
}
int
fs_chmod(const char *modestr, const char *path)
{
	mode_t m;
	mode_t mask = getumask();
    struct stat st;
    if (stat(path, &st))
        return 1;
    if ((m = parsemode(modestr, st.st_mode, mask)) == -1)
        return 1;
    if (chmod(path, m)) {
        weprintf("chmod %s:", path);
        return 1;
    }
    return 0;
}
static int
size_fn(const char *s1, struct stat *st, const char *s2, size_t depth, void *arg)
{
    *(off_t *)arg += st->st_size;
    return 0;
}
int
fs_csize(const char *src, off_t *s)
{
    if (!s)
        return 1;
    *s = 0;
    return recurse(src, NULL, 0, (size_fn), 0, s);
}
