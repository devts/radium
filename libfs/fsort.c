#include "../all.h"

static int fsort_type=SORT_BASENAME; /*this whole thing is NOT THREADSAFE*/
static int fsort_icase = 1;
static int fsort_reverse = 0;
static int fsort_dirs_first=1;

static int
vercmp(int ic, const char *s, const char *t)
{
    int diff, first_diff;
    while (*s || *t) {
        first_diff = 0;
        while ((*s && !isdigit(*s)) || (*t && !isdigit(*t))) {
            diff = ic ? (tolower(*s) - tolower(*t)) : (*s - *t);
            if (diff)
                return diff;
            s++, t++;
        }
        while (*s == '0')
            s++;
        while (*t == '0')
            t++;
        while (isdigit(*s) && isdigit(*t)) {
            if (!first_diff)
                first_diff= *s - *t;
            s++, t++;
        }
        if (isdigit(*s))
            return 1;
        if (isdigit(*t))
            return -1;
        if (first_diff)
            return first_diff;
    }
    return 0;
}

static int
fsort_cmp(const void *va, const void *vb)
{
    fileinfo *a = (fileinfo*) va;
    fileinfo *b = (fileinfo*) vb;
    if (fsort_dirs_first) {
        if (S_ISDIR(a->st_t.st_mode) && !S_ISDIR(b->st_t.st_mode))
            return -1;
        if (S_ISDIR(b->st_t.st_mode) && !S_ISDIR(a->st_t.st_mode))
            return 1;
    }
    switch(fsort_type){
case SORT_NATURAL:
        return (fsort_reverse ? -1 : 1) * vercmp(fsort_icase, a->name, b->name);
case SORT_BASENAME:
        return (fsort_reverse ? -1 : 1) * (fsort_icase ? strcasecmp : strcmp) (a->name, b->name);
case SORT_MTIME:
        return (fsort_reverse ? -1 : 1) * NUMCMP(a->st.st_mtime, b->st.st_mtime);
case SORT_CTIME:
        return (fsort_reverse ? -1 : 1) * NUMCMP(a->st.st_ctime, b->st.st_ctime);
case SORT_ATIME:
        return (fsort_reverse ? -1 : 1) * NUMCMP(a->st.st_atime, b->st.st_atime);
case SORT_SIZE:
        return (fsort_reverse ? -1 : 1) * NUMCMP(a->st.st_size, b->st.st_size);
    }
    return 0;
}

void
fsort(sort s, fileinfo* files, size_t len)
{
    fsort_type = s.type;
    fsort_icase = s.icase;
    fsort_reverse = s.reverse;
    fsort_dirs_first = s.dirs_first;
    qsort(files, len, sizeof(fileinfo), (fsort_cmp));
}
