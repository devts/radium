#include "../all.h"

static int
simplify_path(char *path)
{
	char *cur;
	char *t;
	char *start = path + 1;

	if (!*path || *path != '/')
		return 1;

	for (cur = t = start; ; ) {
		/* treat multiple '/'s as one '/' */
		while (*t == '/')
			t++;

		if (*t == '\0') {
			*cur = '\0';
			break;
		}

		if (t[0] == '.') {
			if (!t[1] || t[1] == '/') {
				t += 1;
				continue;
			} else if (t[1] == '.' && (!t[2] || t[2] == '/')) {
				if (cur != start)
                    while (--cur > start && *cur != '/')
						;
				t += 2;
				continue;
			}
		}

		if (cur != start)
			*cur++ = '/';

		/* find/copy next component of pathname */
		while (*t && *t != '/')
			*cur++ = *t++;
	}
    return 0;
}

char *
canonic_path(const char *dir, const char *arg)
{
    char *res, *tmp;
    if (!arg)
        return 0;
    size_t al = (arg ? strlen(arg) : 0);
    if (al==0 || (al==1 && arg[0] == '~')) {
        char *homedir = getenv("HOME");
        if(!homedir)
            return 0;
        res = estrdup(homedir);
    } else if ( al >=2 && arg[0]=='~' && arg[1]=='/') {
        char *homedir = getenv("HOME");
        if (!homedir)
            return 0;
        res = pathmcat(homedir, arg + 1);
    } else if (arg[0]=='/') {
        res = estrdup(arg);
    } else {
        if (!dir)
            return 0;
        res = pathmcat(dir, arg);
    }
    if (simplify_path(res)) {
        free(res);
        return 0;
    }
    if (res[0] && res[1]) {
        tmp = strmcat(res, "/");
        free(res);
        res = tmp;
    }
    return res;
}
