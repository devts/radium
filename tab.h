typedef struct tab tab;
struct tab
{
    size_t mrow;
    size_t mcol;

    dirview *dv;
    dirview **dv_hist;
    size_t dv_hist_size;

    char **hl_hist;
    size_t hl_hist_size;
    size_t hl_pos;
};

tab *tab_new(char *cwd, char *arg);
int tab_del(tab *t);
int tab_cd_rel(tab *t, char* wd, char* arg);
int tab_cd(tab *t, char* arg);
int tab_H_cd(tab *t, size_t s);
int tab_L_cd(tab *t, size_t s);
int tab_print(tab *t, WINDOW *w);
void tab_bring_cur_in_view(tab *t);
int tab_move_to(tab *t, size_t n);
int tab_move_rel(tab *t, int n);
void tab_move_top(tab *t);
void tab_move_middle(tab *t);
void tab_move_bottom(tab *t);
void tab_page_up(tab *t, size_t n, size_t m, size_t sl);
void tab_page_down(tab *t, size_t n, size_t m, size_t sl);
void tab_cur_to_middle(tab *t);
void tab_cur_to_top(tab *t);
void tab_cur_to_bottom(tab *t);
int tab_enter_cur(tab *t);
int tab_search_next(tab *t, char *reg);
int tab_search_prev(tab *t, char *reg);
