#include "all.h"

dirview *dv_new(char* dir)
{
    dirview *dv;
    dv = ecalloc(1, sizeof(dirview));
    dv->dir = estrdup(dir);
    dv->selection = NULL;
    dv->err = 2;

    dv->st.type = sortprefs.type;
    dv->st.icase = sortprefs.icase;
    dv->st.reverse = sortprefs.reverse;
    dv->st.dirs_first = sortprefs.dirs_first;

    if(dv_update(dv, 1) > 0)
    {
        free(dv->dir);
        free(dv);
        return NULL;
    }
    return dv;
}
void dv_del(dirview *dv)
{
    dv_free_dirlist(dv);
    dv_empty_selection(dv);
    if(dv->cur.name)
        free(dv->cur.name);
    if(dv->off.name)
        free(dv->off.name);
    if(dv->dir)
        free(dv->dir);
    free(dv);
}


/*RETURN VALUE: -1= nothing to do, 0=updated, 1=error*/
int dv_update(dirview *dv, int force)
{
    struct stat st;
    if (stat(dv->dir,&st) || !S_ISDIR(st.st_mode)) {
        dv_free_dirlist(dv);
        return (dv->err = 1);
    }
    if (!force && !dv->err && dv->dir_mtim.tv_nsec == st.st_mtim.tv_nsec)
        return -1;
    dv->dir_mtim=st.st_mtim;
    dv_free_dirlist(dv);
    if (getdirlist(dv->dir, &dv->dirlist, &dv->dirlist_size))
        return (dv->err = 1);
    dv_sort(dv, dv->st);
    return (dv->err = 0);
}

static int
selcmp(const void *a, const void *b)
{
    return NUMCMP(((selitem*)a)->n, ((selitem*)b)->n);
}
static int dv_selection_remove(dirview *dv, size_t i);
void
dv_sort(dirview *dv, sort s)
{
    dv->st = s;
    fsort(dv->st, dv->dirlist, dv->dirlist_size);
    dv_update_cur(dv, dv->cur.name);
    dv_update_off(dv, dv->off.name);
    if(!dv->selection)
        return;
    size_t i;
    for(i = 0; i < dv->selection_size; i++) {
        dv_update_selitem_name(dv, dv->selection + i, dv->selection[i].name, 0);
        if(!dv->selection[i].name) {
            dv_selection_remove(dv, i);
            i--;
        }
    }
    if(!dv->selection)
        return;
    qsort(dv->selection, dv->selection_size, sizeof(selitem), selcmp);
    dv->selection = reallocarray(dv->selection, dv->dirlist_size, sizeof(selitem));
}

void 
dv_free_dirlist(dirview *dv)
{
    if (!dv->err)
        dv->err = 2;
    if(dv->dirlist)
    {
        for(int i = 0; i < dv->dirlist_size; i++)
        {
            if(dv->dirlist[i].name)
                free(dv->dirlist[i].name);
        }
        free(dv->dirlist);
        dv->dirlist = NULL;
    }
    dv->dirlist_size = 0;
}

int dv_update_cur(dirview *dv, char* cur)
{
    return dv_update_selitem_name(dv, &dv->cur, cur, 1);
}
int dv_update_off(dirview *dv, char* off)
{
    return dv_update_selitem_name(dv, &dv->off, off, 1);
}
int dv_update_ncur(dirview *dv, size_t ncur)
{
    return dv_update_selitem_n(dv, &dv->cur, ncur);
}
int dv_update_noff(dirview *dv, size_t noff)
{
    return dv_update_selitem_n(dv, &dv->off, noff);
}
int 
dv_update_selitem_name(dirview *dv, selitem *sel, char* name, int resel)
{
    if(name)
    {
        for(size_t i = 0; i < dv->dirlist_size ; i++)
        {
           if(!strcmp(dv->dirlist[i].name, name))
           {
               sel->n = i;
               if (sel->name != name) {
                   free(sel->name);
                   sel->name = estrdup(name);
               }
               return 0;
           }
        }
    }
    if(resel && dv->dirlist_size > 0 )
    {
        sel->n = sel->n < dv->dirlist_size ? sel->n : dv->dirlist_size - 1;
        free(sel->name);
        sel->name = estrdup(dv->dirlist[sel->n].name);
    }
    else
    {
        sel->n = 0;
        if (sel->name)
            free(sel->name);
        sel->name = NULL;
    }
    return 1;
}
int
dv_update_selitem_n(dirview *dv, selitem *sel, size_t nsel)
{
    if( nsel < dv->dirlist_size )
    {
        sel->n = nsel;
        free(sel->name);
        sel->name = estrdup(dv->dirlist[nsel].name);
        return 0;
    }
    return 1;
}

static regex_t preg;
static int h_preg=0;
/*passing NULL for reg means ue previous regex
 * and get the next match after nsel*/
/*returns 0 on found result, 1 on error, 2 on no next result found*/
int 
dv_search_next(dirview *dv, char *reg, size_t *nr)
{
    if(!h_preg && !reg)
        return 1;
    if(reg)
    {
       if(h_preg)
            regfree(&preg); 
       if(regcomp(&preg, reg, REG_EXTENDED))
            return 1;
        h_preg = 1;
    }
    size_t ncur = dv->cur.n;
    size_t dirl = dv->dirlist_size;
    fileinfo* dirs = dv->dirlist;
    for(size_t i = ncur + (reg ? 0 : 1 ); i < dirl; i++)
    {
        if(!regexec(&preg, dirs[i].name, 0, NULL, 0))
        {
            *nr = i;
            return 0;
        }
    }
    for(size_t i = 0; i < ncur; i++)
    {
        if(!regexec(&preg, dirs[i].name, 0, NULL, 0))
        {
            *nr = i;
            return 0;
        }
    }
    return 2;
}
int 
dv_search_prev(dirview *dv, char *reg, size_t *pr)
{
    if(!h_preg && !reg)
        return 1;
    if(reg)
    {
       if(h_preg)
            regfree(&preg); 
       if(regcomp(&preg, reg, REG_EXTENDED))
            return 1;
        h_preg = 1;
    }
    size_t ncur = dv->cur.n;
    size_t dirl = dv->dirlist_size;
    fileinfo* dirs = dv->dirlist;
    if(dirl == 0)
        return 2;
    if(ncur > 0)
        for(size_t i = ncur - (reg ? 0 : 1 ); 1 ; i--)
        {
            if(!regexec(&preg, dirs[i].name, 0, NULL, 0))
            {
                *pr = i;
                return 0;
            }
            if(i == 0)
                break;
        }
    for(size_t i = dirl - 1; i > ncur; i--)
    {
        if(!regexec(&preg, dirs[i].name, 0, NULL, 0))
        {
            *pr = i;
            return 0;
        }
    }
    return 2;
}

static int
dv_selection_insert(dirview *dv, size_t n, size_t i)
{
    if (!dv->selection) {
        dv->selection = calloc(dv->dirlist_size, sizeof(selitem));
    }
    if (i > dv->selection_size)
        return 1;
    if (i != dv->selection_size)
        memmove(dv->selection + i + 1, dv->selection + i, (dv->selection_size - i)*sizeof(selitem));
    dv->selection_size++;
    dv->selection[i].n = n;
    dv->selection[i].name = estrdup(dv->dirlist[n].name);
    return 0;
}

static int
dv_selection_remove(dirview *dv, size_t i)
{
    if (i >= dv->selection_size)
        return 1;
    dv->selection[i].n = 0;
    if (dv->selection[i].name)
        free(dv->selection[i].name);
    dv->selection[i].name = 0;
    memmove(dv->selection + i, dv->selection + i + 1, (dv->selection_size - i - 1)*sizeof(selitem));
    dv->selection_size--;
    if (!dv->selection_size) {
        free(dv->selection);
        dv->selection = 0;
    }
    return 0;
}

static int
dv_manage_selection(dirview *dv, size_t n, int insert, int toggle, int delete)
{
    if(n >= dv->dirlist_size)
        return 1;
    size_t i;
    for (i = 0; i < dv->selection_size && dv->selection[i].n < n; i++) { }
    if (i == dv->selection_size) {
        if (delete)
            return 0;
        return dv_selection_insert(dv, n, i);
    }
    if (dv->selection[i].n == n) {
        if (insert)
            return 0;
        return dv_selection_remove(dv, i);
    }
    if (delete)
        return 0;
    return dv_selection_insert(dv, n, i);
}
int
dv_select_item(dirview *dv, size_t n)
{
    return dv_manage_selection(dv, n, 1, 0, 0);
}
int
dv_toggleselect_item(dirview *dv, size_t n)
{
    return dv_manage_selection(dv, n, 0, 1, 0);
}
int
dv_unselect_item(dirview *dv, size_t n)
{
    return dv_manage_selection(dv, n, 0, 0, 1);
}
int
dv_empty_selection(dirview *dv)
{
    if (dv->selection) {
        size_t i;
        for (i = 0; i < dv->selection_size; i++)
            free(dv->selection[i].name);
        free(dv->selection);
        dv->selection = 0;
        dv->selection_size = 0;
    }
    return 0;
}
int
dv_regsel(dirview *dv, char *reg)
{
    regex_t creg;
    if (regcomp(&creg, reg, REG_EXTENDED))
        return 1;
    size_t dirl = dv->dirlist_size;
    fileinfo* dirs = dv->dirlist;
    size_t i;
    for (i = 0; i < dirl; i++)
        if(!regexec(&creg, dirs[i].name, 0, NULL, 0))
            dv_select_item(dv, i);
    regfree(&creg);
    return 0;
}
int
dv_iterate_as(dirview *dv, int (*func)(char *path, fileinfo *fi, void *v), void *v)
{
    char *buf;
    int res = 0;
    if(dv->selection)
    {
        size_t i;
        for(i = 0; i < dv->selection_size; i++)
        {
            buf = pathmcat(dv->dir, dv->selection[i].name);
            res |= func(buf, dv->dirlist + dv->selection[i].n, v);
            free(buf);
        }
    }
    else
    {
        buf = pathmcat(dv->dir, dv->cur.name);
        res |= func(buf, dv->dirlist + dv->cur.n, v);
        free(buf);
    }
    return res;
}
