#include "all.h"

/*radium prefs*/
int startdp = 0;
char *new_tab_cwd = "~";

/*tab prefs*/
size_t so = 8;
int tab_vind = 0;
int tab_indF = 0;
int tab_nflag = 0;
char *tab_time_fmt = "%Y-%m-%d %H:%M";
char *tab_dir_str = "dir";

/*tabpane prefs*/
int ra_otnb = 1;

/*sortprefs*/
sort sortprefs = { .type = SORT_NATURAL, .icase = 1, .reverse = 0, .dirs_first = 1};

/*bookmarks*/
mark _bmrks[] = {
    /*key   location*/
    {'k',   "/tmp"},
    {'r',   "/mnt"},
};
mark *bmrks = _bmrks;
size_t s_bmrks = LEN(_bmrks);

/*key from char*/
#define KFC(x) {.utfk = 1, .ku = { .r = (Rune)(x) }}
/*key from int*/
#define KFI(x) {.utfk = 0, .ku = { .k = (int)(x) }}
#define CTRL(a) ((a) - 'a' + 1)

/* keyconfig*/
/* normal mode keybindings */
keybind normal_kbs_[] = {
    /* key                                  s_keys          s_post      function                     argument                            */
    { (key[]){ KFC('j')},                   1,              0,          cmd_jk,                      {.i = 1  },                          },
    { (key[]){ KFC('k')},                   1,              0,          cmd_jk,                      {.i = -1 },                          },
    { (key[]){ KFC('h')},                   1,              0,          cmd_cd_parent,               {.i = 1  },                          },
    { (key[]){ KFC('l')},                   1,              0,          cmd_enter_cur,               {.i = 1  },                          },
    { (key[]){ KFC('s')},                   1,              0,          cmd_open_shell,              {.i = 0  },                          },
    { (key[]){ KFC('q')},                   1,              0,          cmd_tab_close,               {.i = 0  },                          },
    { (key[]){ KFC('e')},                   1,              0,          cmd_edit,                    {.i = 0  },                          },
    { (key[]){ KFC('S')},                   1,              0,          cmd_open_shell,              {.i = 0  },                          },
    { (key[]){ KFC('G')},                   1,              0,          cmd_G,                       {.i = 0  },                          },
    { (key[]){ KFC('/')},                   1,              0,          cmd_search,                  {.i = 0  },                          },
    { (key[]){ KFC('n')},                   1,              0,          cmd_search_next,             {.i = 0  },                          },
    { (key[]){ KFC('N')},                   1,              0,          cmd_search_prev,             {.i = 0  },                          },
    { (key[]){ KFC(':')},                   1,              0,          cmd_entercmd,                {.i = 0  },                          },
    { (key[]){ KFC('a')},                   1,              0,          cmd_list_tar,                {.i = 0  },                          },
    { (key[]){ KFC('\'')},                  1,              1,          cmd_jumpmark,                {.i = 0  },                          },
    { (key[]){ KFC('m')},                   1,              1,          cmd_setmark,                 {.i = 0  },                          },
    { (key[]){ KFC(' ')},                   1,              0,          cmd_togglesel,               {.i = 0  },                          },
    { (key[]){ KFC('t')},                   1,              0,          cmd_tab_open,                {.v = "~"},                          },
    { (key[]){ KFC('T')},                   1,              0,          cmd_tab_open,                {.v = 0  },                          },
    { (key[]){ KFC('w')},                   1,              0,          cmd_toggle_dualpane,         {.i = 0  },                          },
    { (key[]){ KFC('\t')},                  1,              0,          cmd_toggle_selpane,          {.i = 0  },                          },
    { (key[]){ KFC('r')},                   1,              0,          cmd_force_update,            {.i = 0  },                          },
    { (key[]){ KFC('H')},                   1,              0,          cmd_H,                       {.i = 0  },                          },
    { (key[]){ KFC('L')},                   1,              0,          cmd_L,                       {.i = 0  },                          },
    { (key[]){ KFC('W')},                   1,              0,          cmd_write_dirfile,           {.i = 0  },                          },
    { (key[]){ KFC(CTRL('b'))},             1,              0,          cmd_page_up,                 {.i = 0  },                          },
    { (key[]){ KFC(CTRL('f'))},             1,              0,          cmd_page_down,               {.i = 0  },                          },
    { (key[]){ KFC(CTRL('u'))},             1,              0,          cmd_scroll_up,               {.i = 0  },                          },
    { (key[]){ KFC(CTRL('d'))},             1,              0,          cmd_scroll_down,             {.i = 0  },                          },
    { (key[]){ KFI(KEY_DOWN)},              1,              0,          cmd_jk,                      {.i = 1  }},
    { (key[]){ KFI(KEY_UP)},                1,              0,          cmd_jk,                      {.i = -1 }},
    { (key[]){ KFI(KEY_LEFT)},              1,              0,          cmd_cd_parent,               {.i = 1  }},
    { (key[]){ KFI(KEY_RIGHT)},             1,              0,          cmd_enter_cur,               {.i = 1  }},
    { (key[]){ KFC('g'), KFC('g')},         2,              0,          cmd_gg,                      {.i = 0  },                          },
    { (key[]){ KFC('g'), KFC('h')},         2,              0,          cmd_cd_home,                 {.i = 0  },                          },
    { (key[]){ KFC('g'), KFC('t')},         2,              0,          cmd_tab_next,                {.i = 0  },                          },
    { (key[]){ KFC('g'), KFC('T')},         2,              0,          cmd_tab_prev,                {.i = 0  },                          },
    { (key[]){ KFC('y'), KFC('y')},         2,              0,          cmd_copy,                    {.i = 0  },                          },
    { (key[]){ KFC('y'), KFC('p')},         2,              0,          cmd_yank_cur_path,           {.i = 0  },                          },
    { (key[]){ KFC('y'), KFC('d')},         2,              0,          cmd_yank_cur_dir,            {.i = 0  },                          },
    { (key[]){ KFC('y'), KFC('n')},         2,              0,          cmd_yank_cur_name,           {.i = 0  },                          },
    { (key[]){ KFC('d'), KFC('d')},         2,              0,          cmd_cut,                     {.i = 0  },                          },
    { (key[]){ KFC('p'), KFC('p')},         2,              0,          cmd_paste,                   {.i = 0  },                          },
    { (key[]){ KFC('d'), KFC('c')},         2,              0,          cmd_csize,                   {.i = 0  },                          },
    { (key[]){ KFC('o'), KFC('n')},         2,              0,          cmd_order,                   {.v = (int[]){SORT_NATURAL, 0} },    },
    { (key[]){ KFC('o'), KFC('b')},         2,              0,          cmd_order,                   {.v = (int[]){SORT_BASENAME, 0} },   },
    { (key[]){ KFC('o'), KFC('m')},         2,              0,          cmd_order,                   {.v = (int[]){SORT_MTIME, 1} },      },
    { (key[]){ KFC('o'), KFC('s')},         2,              0,          cmd_order,                   {.v = (int[]){SORT_SIZE, 1} },       },
    { (key[]){ KFC('o'), KFC('r')},         2,              0,          cmd_order_toggle_reverse,    {.i =  0  },                         },
    { (key[]){ KFC('o'), KFC('N')},         2,              0,          cmd_order,                   {.v = (int[]){SORT_NATURAL, 1} },    },
    { (key[]){ KFC('o'), KFC('B')},         2,              0,          cmd_order,                   {.v = (int[]){SORT_BASENAME, 1} },   },
    { (key[]){ KFC('o'), KFC('M')},         2,              0,          cmd_order,                   {.v = (int[]){SORT_MTIME, 0} },      },
    { (key[]){ KFC('o'), KFC('S')},         2,              0,          cmd_order,                   {.v = (int[]){SORT_SIZE, 0} },       },
    { (key[]){ KFC('z'), KFC('h')},         2,              0,          cmd_move_top,                {.i = 0  },                          },
    { (key[]){ KFC('z'), KFC('m')},         2,              0,          cmd_move_middle,             {.i = 0  },                          },
    { (key[]){ KFC('z'), KFC('l')},         2,              0,          cmd_move_bottom,             {.i = 0  },                          },
    { (key[]){ KFC('z'), KFC('z')},         2,              0,          cmd_cur_to_middle,           {.i = 0  },                          },
    { (key[]){ KFC('z'), KFC('t')},         2,              0,          cmd_cur_to_top,              {.i = 0  },                          },
    { (key[]){ KFC('z'), KFC('b')},         2,              0,          cmd_cur_to_bottom,           {.i = 0  },                          },
    { (key[]){ KFC('+'), KFC('x')},         2,              0,          cmd_chmod,                   {.v = "+x"},                         },
    { (key[]){ KFC('-'), KFC('x')},         2,              0,          cmd_chmod,                   {.v = "-x"},                         },
};
keybind *normal_kbs = normal_kbs_;
size_t s_normal_kbs = LEN(normal_kbs_);
keybind *normal_fallthr = 0;

keybind kb_prompt_ins =
    { (key[]){ KFC('j')},                   1,              0,          cmd_prompt_ins,                      {.i =1  },                          };

/* cmd mode keybindings */
keybind cmd_kbs_[] = {
	/* key                                  s_keys          s_post      function                     argument                            */
    { (key[]){ KFC(27)},                    1,              0,          cmd_prompt_exit,             {.i =1  },                          },
    { (key[]){ KFI(KEY_LEFT)},              1,              0,          cmd_prompt_curs_left,        {.i =1  },                          },
    { (key[]){ KFI(KEY_RIGHT)},             1,              0,          cmd_prompt_curs_right,       {.i =1  },                          },
    { (key[]){ KFC(CTRL('a'))},             1,              0,          cmd_prompt_curs_start,       {.i =1  },                          },
    { (key[]){ KFC(CTRL('e'))},             1,              0,          cmd_prompt_curs_end,         {.i =1  },                          },
    { (key[]){ KFI(KEY_BACKSPACE)},         1,              0,          cmd_prompt_curs_bs,          {.i =1  },                          },
    { (key[]){ KFC(127)},                   1,              0,          cmd_prompt_curs_bs,          {.i =1  },                          },
    { (key[]){ KFI(KEY_DC)},                1,              0,          cmd_prompt_curs_dc,          {.i =1  },                          },
    { (key[]){ KFC(CTRL('u'))},             1,              0,          cmd_prompt_curs_dl,          {.i =1  },                          },
    { (key[]){ KFC(CTRL('k'))},             1,              2,          cmd_prompt_ins_digraph,      {.i =1  },                          },
    { (key[]){ KFC('\t')},                  1,              0,          cmd_prompt_complete,         {.i =1  },                          },
    { (key[]){ KFC('\n')},                  1,              0,          cmd_prompt_exec,             {.i =1  },                          },
    { (key[]){ KFI(KEY_UP)},                1,              0,          cmd_switch_hist,             {.i =-1 },                          },
    { (key[]){ KFI(KEY_DOWN)},              1,              0,          cmd_switch_hist,             {.i =1  },                          },
};
keybind *cmd_kbs = cmd_kbs_;
size_t s_cmd_kbs = LEN(cmd_kbs_);
keybind *cmd_fallthr = &kb_prompt_ins;

/* search mode keybindings */
keybind search_kbs_[] = {
	/* key                                  s_keys          s_post      function                     argument                            */
    { (key[]){ KFC(27)},                    1,              0,          cmd_prompt_exit,             {.i =1  },                          },
    { (key[]){ KFI(KEY_LEFT)},              1,              0,          cmd_prompt_curs_left,        {.i =1  },                          },
    { (key[]){ KFI(KEY_RIGHT)},             1,              0,          cmd_prompt_curs_right,       {.i =1  },                          },
    { (key[]){ KFC(CTRL('a'))},             1,              0,          cmd_prompt_curs_start,       {.i =1  },                          },
    { (key[]){ KFC(CTRL('e'))},             1,              0,          cmd_prompt_curs_end,         {.i =1  },                          },
    { (key[]){ KFI(KEY_BACKSPACE)},         1,              0,          cmd_prompt_curs_bs,          {.i =1  },                          },
    { (key[]){ KFC(127)},                   1,              0,          cmd_prompt_curs_bs,          {.i =1  },                          },
    { (key[]){ KFI(KEY_DC)},                1,              0,          cmd_prompt_curs_dc,          {.i =1  },                          },
    { (key[]){ KFC(CTRL('u'))},             1,              0,          cmd_prompt_curs_dl,          {.i =1  },                          },
    { (key[]){ KFC(CTRL('k'))},             1,              2,          cmd_prompt_ins_digraph,      {.i =1  },                          },
    { (key[]){ KFC('\t')},                  1,              0,          cmd_prompt_complete,         {.i =1  },                          },
    { (key[]){ KFC('\n')},                  1,              0,          cmd_prompt_search_submit,    {.i =1  },                          },
    { (key[]){ KFI(KEY_UP)},                1,              0,          cmd_switch_hist,             {.i =-1 },                          },
    { (key[]){ KFI(KEY_DOWN)},              1,              0,          cmd_switch_hist,             {.i =1  },                          },
};
keybind *search_kbs = search_kbs_;
size_t s_search_kbs = LEN(search_kbs_);
keybind *search_fallthr = &kb_prompt_ins;

/*cmdline prefs*/
Cmd _cmds[] = {
	/* name         s_name      func    */
	{ "cd",         2,          cmd_cd          },
	{ "rr",         2,          cmd_regex_rename},
	{ "touch",      5,          cmd_touch       },
	{ "mkdir",      5,          cmd_mkdir       },
	{ "delete",     6,          cmd_delete      },
	{ "rename",     6,          cmd_rename      },
	{ "s",          1,          cmd_regsel      },
	{ "cs",         2,          cmd_clearsel    },
	{ "ws",         2,          cmd_writesel    },
	{ "ow",         2,          cmd_open_with   },
	{ "open_with",  9,          cmd_open_with   },
};
Cmd *cmds = _cmds;
size_t s_cmds = LEN(_cmds);

/*colorscheme*/
int initcolorscheme(void)
{
   scheme.DEFAULT_ATTR = getcolor(COLOR_WHITE,COLOR_BLACK);
   scheme.ERROR_ATTR = getcolor(COLOR_RED, COLOR_BLACK) | A_BOLD;
   scheme.TAB_INACTIVE_ATTR = getcolor(COLOR_WHITE,COLOR_BLACK) | A_BOLD;
   scheme.TAB_ACTIVE_ATTR = getcolor(COLOR_WHITE,COLOR_GREEN) | A_BOLD ;
   scheme.CWD_ATTR = getcolor(COLOR_YELLOW,COLOR_BLACK);
   scheme.MODE_ATTR = getcolor(COLOR_CYAN,COLOR_BLACK);
   scheme.DIR_ATTR = getcolor(COLOR_BLUE,COLOR_BLACK) | A_BOLD;
   scheme.FILE_ATTR = getcolor(COLOR_WHITE,COLOR_BLACK);
   scheme.SUID_ATTR = getcolor(COLOR_RED,COLOR_BLACK) | A_REVERSE;
   scheme.EXECUTABLE_ATTR = getcolor(COLOR_GREEN,COLOR_BLACK) | A_BOLD;
   scheme.LNK_ATTR = getcolor(COLOR_CYAN,COLOR_BLACK) | A_BOLD;
   scheme.FIFO_ATTR = getcolor(COLOR_YELLOW,COLOR_BLACK);
   scheme.SOCKET_ATTR = getcolor(COLOR_MAGENTA,COLOR_BLACK) | A_BOLD;
   scheme.DEVICE_ATTR = getcolor(COLOR_YELLOW,COLOR_BLACK) | A_BOLD;
   scheme.CUR_OR_ATTR = A_REVERSE | A_BOLD;
   scheme.SELECTED_ATTR = getcolor(COLOR_YELLOW,COLOR_BLACK)| A_BOLD;
   return 0;
}

char *yank_pipe_file = "xsel";
char **yank_pipe_arg = (char*[]){"xsel", "-i", NULL};

/*fileopenprefs*/
opener open_edit   = {.fork = 0, .append_all = 0, .file = "vim",     .argc = 2, .argv = (char*[]){"vim", NULL } };
opener open_pdf    = {.fork = 1, .append_all = 0, .file = "zathura", .argc = 2, .argv = (char*[]){"zathura", NULL } };
opener open_pic    = {.fork = 1, .append_all = 1, .file = "feh",     .argc = 4, .argv = (char*[]){"feh",  "--start-at", NULL, "-Z.N"} };
opener open_music  = {.fork = 1, .append_all = 0, .file = "mpv",     .argc = 2, .argv = (char*[]){"mpv", NULL } };
opener open_video  = {.fork = 1, .append_all = 0, .file = "mpv",     .argc = 2, .argv = (char*[]){"mpv", NULL } };
opener open_html   = {.fork = 1, .append_all = 0, .file = "firefox", .argc = 2, .argv = (char*[]){"firefox", NULL } };

extrule _rules[] = {
    { .ext = ".c",      .o = &open_edit  },
    { .ext = ".h",      .o = &open_edit  },
    { .ext = ".diff",   .o = &open_edit  },
    { .ext = "makefile",.o = &open_edit  },
    { .ext = ".mk",     .o = &open_edit  },
    { .ext = ".tex",    .o = &open_edit  },
    { .ext = ".sh",     .o = &open_edit  },
    { .ext = ".py",     .o = &open_edit  },
    { .ext = ".pl",     .o = &open_edit  },
    { .ext = ".rb",     .o = &open_edit  },
    { .ext = ".txt",    .o = &open_edit  },
    { .ext = ".csv",    .o = &open_edit  },
    { .ext = ".js",     .o = &open_edit  },
    { .ext = ".css",    .o = &open_edit  },

    { .ext = ".pdf",    .o = &open_pdf   },
    { .ext = ".ps",     .o = &open_pdf   },
    { .ext = ".djvu",   .o = &open_pdf   },

    { .ext = ".jpg",    .o = &open_pic   },
    { .ext = ".jpe",    .o = &open_pic   },
    { .ext = ".jpeg",   .o = &open_pic   },
    { .ext = ".bmp",    .o = &open_pic   },
    { .ext = ".png",    .o = &open_pic   },
    { .ext = ".tif",    .o = &open_pic   },
    { .ext = ".tiff",   .o = &open_pic   },
    { .ext = ".pnm",    .o = &open_pic   },
    { .ext = ".gif",    .o = &open_pic   },
    { .ext = ".webp",   .o = &open_pic   },
    { .ext = ".avif",   .o = &open_pic   },

    { .ext = ".ogg",    .o = &open_music },
    { .ext = ".opus",   .o = &open_music },
    { .ext = ".mp3",    .o = &open_music },
    { .ext = ".wav",    .o = &open_music },
    { .ext = ".wma",    .o = &open_music },
    { .ext = ".m4a",    .o = &open_music },

    { .ext = ".mp4",    .o = &open_video },
    { .ext = ".mpg",    .o = &open_video },
    { .ext = ".flv",    .o = &open_video },
    { .ext = ".mkv",    .o = &open_video },
    { .ext = ".vob",    .o = &open_video },
    { .ext = ".webm",    .o = &open_video },
    { .ext = ".mp4.part",    .o = &open_video },
    { .ext = ".mpg.part",    .o = &open_video },
    { .ext = ".flv.part",    .o = &open_video },
    { .ext = ".mkv.part",    .o = &open_video },
    { .ext = ".vob.part",    .o = &open_video },
    { .ext = ".webm.part",    .o = &open_video },

    { .ext = ".html",   .o = &open_html  },
};
extrule *rules = _rules;
size_t rules_len = LEN(_rules);
