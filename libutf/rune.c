#include "../all.h"
#include "rune_gen.h"
#define nelem(x) (LEN(x))

int
rune1cmp(const void *v1, const void *v2) {
  Rune r1 = *(Rune *)v1, r2 = *(Rune *)v2;

    return r1 - r2;
}

int
rune2cmp(const void *v1, const void *v2) {
    Rune r = *(Rune *)v1, *p = (Rune *)v2;

    if(r >= p[0] && r <= p[1])
        return 0;
    else
        return r - p[0];
}

int
isalnumrune(Rune r) {
    return isalpharune(r) || isdigitrune(r);
}

int
isblankrune(Rune r) {
    return r == ' ' || r == '\t';
}

int
iscntrlrune(Rune r) {
    if(bsearch(&r, cntrl2, nelem(cntrl2), sizeof *cntrl2, &rune2cmp))
        return 1;
    return 0;
}

int
isdigitrune(Rune r) {
    if(bsearch(&r, digit2, nelem(digit2), sizeof *digit2, &rune2cmp))
        return 1;
    return 0;
}

int
isgraphrune(Rune r) {
    return !isspacerune(r) && isprintrune(r);
}

int
isprintrune(Rune r) {
    return !iscntrlrune(r) && (r != 0x2028) && (r != 0x2029) &&
           ((r < 0xFFF9) || (r > 0xFFFB));
}

int
ispunctrune(Rune r) {
    return isgraphrune(r) && !isalnumrune(r);
}

int
isspacerune(Rune r) {
    if(bsearch(&r, space2, nelem(space2), sizeof *space2, &rune2cmp))
        return 1;
    if(bsearch(&r, space1, nelem(space1), sizeof *space1, &rune1cmp))
        return 1;
    return 0;
}

int
istitlerune(Rune r) {
    if(bsearch(&r, title2, nelem(title2), sizeof *title2, &rune2cmp))
        return 1;
    if(bsearch(&r, title1, nelem(title1), sizeof *title1, &rune1cmp))
        return 1;
    return 0;
}

int
isxdigitrune(Rune r) {
    return (r >= '0' && (r - '0') < 10) || (r >= 'a' && (r - 'a') < 6);
}

int
isalpharune(Rune r) {
    Rune *match;

    if((match = bsearch(&r, alpha3, nelem(alpha3), sizeof *alpha3, &rune2cmp)))
        return !((r - match[0]) % 2);
    if(bsearch(&r, alpha2, nelem(alpha2), sizeof *alpha2, &rune2cmp))
        return 1;
    if(bsearch(&r, alpha1, nelem(alpha1), sizeof *alpha1, &rune1cmp))
        return 1;
    return 0;
}

int
islowerrune(Rune r) {
    Rune *match;

    if((match = bsearch(&r, lower4, nelem(lower4), sizeof *lower4, &rune2cmp)))
        return !((r - match[0]) % 2);
    if(bsearch(&r, lower2, nelem(lower2), sizeof *lower2, &rune2cmp))
        return 1;
    if(bsearch(&r, lower1, nelem(lower1), sizeof *lower1, &rune1cmp))
        return 1;
    return 0;
}

Rune
toupperrune(Rune r) {
    Rune *match;

    match = bsearch(&r, lower4, nelem(lower4), sizeof *lower4, &rune2cmp);
    if (match)
        return ((r - match[0]) % 2) ? r : r - 1;
    match = bsearch(&r, lower2, nelem(lower2), sizeof *lower2, &rune2cmp);
    if (match)
        return match[2] + (r - match[0]);
    match = bsearch(&r, lower1, nelem(lower1), sizeof *lower1, &rune1cmp);
    if (match)
        return match[1];
    return r;
}

int
isupperrune(Rune r) {
    Rune *match;

    if((match = bsearch(&r, upper3, nelem(upper3), sizeof *upper3, &rune2cmp)))
        return !((r - match[0]) % 2);
    if(bsearch(&r, upper2, nelem(upper2), sizeof *upper2, &rune2cmp))
        return 1;
    if(bsearch(&r, upper1, nelem(upper1), sizeof *upper1, &rune1cmp))
        return 1;
    return 0;
}

Rune
tolowerrune(Rune r) {
    Rune *match;

    match = bsearch(&r, upper3, nelem(upper3), sizeof *upper3, &rune2cmp);
    if (match)
        return ((r - match[0]) % 2) ? r : r + 1;
    match = bsearch(&r, upper2, nelem(upper2), sizeof *upper2, &rune2cmp);
    if (match)
        return match[2] + (r - match[0]);
    match = bsearch(&r, upper1, nelem(upper1), sizeof *upper1, &rune1cmp);
    if (match)
        return match[1];
    return r;
}
