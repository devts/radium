/*radium prefs*/
int startdp;
char *new_tab_cwd;

/*tab prefs*/
size_t so;
int tab_vind;
int tab_indF;
int tab_nflag;
char *tab_time_fmt;
char *tab_dir_str;

/*tabpane prefs*/
int ra_otnb;

/*sortprefs*/
sort sortprefs;

/*bookmarks*/
mark *bmrks;
size_t s_bmrks;

/*keyconfig*/
keybind *normal_kbs;
size_t s_normal_kbs;
keybind *normal_fallthr;

keybind *cmd_kbs;
size_t s_cmd_kbs;
keybind *cmd_fallthr;

keybind *search_kbs;
size_t s_search_kbs;
keybind *search_fallthr;

/*cmdline prefs*/
Cmd *cmds;
size_t s_cmds;

/*colorscheme*/
int initcolorscheme(void);

char *yank_pipe_file;
char **yank_pipe_arg;

/*fileopenprefs*/
extrule *rules;
size_t rules_len;
