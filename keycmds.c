#include "all.h"

/*Normal mode cmds*/
static void
empty_y_sel(void)
{
    if(y_sel)
    {
        for(size_t i = 0; i < y_sels; i++)
            free(y_sel[i]);
        y_sels = 0;
        free(y_sel);
        y_sel = NULL;
    }
    y_status = Y_NONE;
}
static int
add_y_sel(char *path, fileinfo *fi, void *v)
{
    y_sel = ereallocarray(y_sel, ++y_sels, sizeof(char*)); 
    y_sel[y_sels - 1] = estrdup(path);
    return 0;
}
static int
set_y_sel(dirview *dv)
{
    if(y_sel)
        empty_y_sel();
    return dv_iterate_as(dv, add_y_sel, 0);
}
void
cmd_copy(unsigned int pre, key *post, const Arg *arg, void *v)
{
    tabpane *tp = v;
    if (set_y_sel(tp->seltab->dv))
    {
        empty_y_sel();
        weprintf("set_y_sel failed");
        return;
    }
    y_status = Y_COPY;
    tp_print(tp);
}
void
cmd_cut(unsigned int pre, key *post, const Arg *arg, void *v)
{
    tabpane *tp = v;
    if (set_y_sel(tp->seltab->dv))
    {
        empty_y_sel();
        weprintf("set_y_sel failed");
        return;
    }
    y_status = Y_CUT;
    tp_print(tp);
}
void
cmd_paste(unsigned int pre, key *post, const Arg *arg, void *v)
{
    tabpane *tp = v;
    if(y_sel)
    {
        for(size_t i = 0; i < y_sels; i++)
        {
            if(y_status == Y_COPY)
            {
                if(fs_cp(y_sel[i], tp->seltab->dv->dir))
                    weprintf("cp %s -> %s failed.", y_sel[i], tp->seltab->dv->dir);
            }
            else if(y_status == Y_CUT)
            {
                if(fs_mv(y_sel[i], tp->seltab->dv->dir))
                    weprintf("mv %s -> %s failed.", y_sel[i], tp->seltab->dv->dir);
            }
            else
            {
                weprintf("unknown operation %s -> %s failed.", y_sel[i], tp->seltab->dv->dir);
                break;
            }
        }
        empty_y_sel();
    }
    dv_update(tp->seltab->dv, 1);
    tp_print(tp);
}
static int
f_chmod(char *path, fileinfo *fi, void *v)
{
    if (fs_chmod((const char *)v, path))
        return weprintf("chmod %s %s failed", (const char *)v, path), 1;
    return 0;
}
void
cmd_chmod(unsigned int pre, key *post, const Arg *arg, void *v)
{
    tabpane *tp = v;
    dv_iterate_as(tp->seltab->dv, f_chmod, (void *)arg->v);
    dv_update(tp->seltab->dv, 1);
    tp_print(tp);
}
static int
f_csize(char *path, fileinfo *fi, void *v)
{
    if(fs_csize(path, &fi->csize))
        return weprintf("csize %s failed", path), 1;
    return 0;
}
void
cmd_csize(unsigned int pre, key *post, const Arg *arg, void *v)
{
    tabpane *tp = v;
    dv_iterate_as(tp->seltab->dv, f_csize, 0);
    tp_print(tp);
}
void
cmd_force_update(unsigned int pre, key *post, const Arg *arg, void *v)
{
    tabpane *tp = v;
    dv_update(tp->seltab->dv, 1);
    tp_print(tp);
}
void
cmd_entercmd(unsigned int pre, key *post, const Arg *arg, void *v)
{
    vcursctx_mode_switch(ctx, mode_cmd);
}
void 
cmd_gg(unsigned int pre, key *post, const Arg *arg, void *v)
{
    tabpane *tp = v;
    tab_move_to(tp->seltab, (pre ? pre - 1 : 0 ));
    tp_print(tp);
}
void 
cmd_G(unsigned int pre, key *post, const Arg *arg, void *v)
{
    tabpane *tp = v;
    tab_move_to(tp->seltab, (pre ? pre - 1 : tp->seltab->dv->dirlist_size - 1 ));
    tp_print(tp);
}
void 
cmd_jk(unsigned int pre, key *post, const Arg *arg, void *v)
{
    tabpane *tp = v;
    tab_move_rel(tp->seltab, arg->i * (pre ? pre : 1 ));
    tp_print(tp);
}
void
cmd_move_top(unsigned int pre, key *post, const Arg *arg, void *v)
{
    tabpane *tp = v;
    tab_move_top(tp->seltab);
    tp_print(tp);
}
void
cmd_move_middle(unsigned int pre, key *post, const Arg *arg, void *v)
{
    tabpane *tp = v;
    tab_move_middle(tp->seltab);
    tp_print(tp);
}
void
cmd_move_bottom(unsigned int pre, key *post, const Arg *arg, void *v)
{
    tabpane *tp = v;
    tab_move_bottom(tp->seltab);
    tp_print(tp);
}
void
cmd_page_up(unsigned int pre, key *post, const Arg *arg, void *v)
{
    tabpane *tp = v;
    tab_page_up(tp->seltab, pre ? pre : 1, 1, 0);
    tp_print(tp);
}
void
cmd_page_down(unsigned int pre, key *post, const Arg *arg, void *v)
{
    tabpane *tp = v;
    tab_page_down(tp->seltab, pre ? pre : 1, 1, 0);
    tp_print(tp);
}
static size_t sl = 0;
void
cmd_scroll_up(unsigned int pre, key *post, const Arg *arg, void *v)
{
    tabpane *tp = v;
    if (pre)
        sl = pre;
    if (sl)
        tab_page_up(tp->seltab, 0, 0, sl);
    else
        tab_page_up(tp->seltab, 1, 2, 0);
    tp_print(tp);
}
void
cmd_scroll_down(unsigned int pre, key *post, const Arg *arg, void *v)
{
    tabpane *tp = v;
    if (pre)
        sl = pre;
    if (sl)
        tab_page_down(tp->seltab, 0, 0, sl);
    else
        tab_page_down(tp->seltab, 1, 2, 0);
    tp_print(tp);
}
void
cmd_cur_to_middle(unsigned int pre, key *post, const Arg *arg, void *v)
{
    tabpane *tp = v;
    tab_cur_to_middle(tp->seltab);
    tp_print(tp);
}
void
cmd_cur_to_top(unsigned int pre, key *post, const Arg *arg, void *v)
{
    tabpane *tp = v;
    tab_cur_to_top(tp->seltab);
    tp_print(tp);
}
void
cmd_cur_to_bottom(unsigned int pre, key *post, const Arg *arg, void *v)
{
    tabpane *tp = v;
    tab_cur_to_bottom(tp->seltab);
    tp_print(tp);
}
void
cmd_enter_cur(unsigned int pre, key *post, const Arg *arg, void *v)
{
    tabpane *tp = v;
    if (tab_enter_cur(tp->seltab))
        weprintf("enter sel failed.");
    tp_print(tp);
}
void
cmd_cd_parent(unsigned int pre, key *post, const Arg *arg, void *v)
{
    tabpane *tp = v;
    if (tab_cd(tp->seltab, "../"))
        weprintf("cd %s failed.", "../");
    tp_print(tp);
}
void
cmd_cd_home(unsigned int pre, key *post, const Arg *arg, void *v)
{
    tabpane *tp = v;
    if (tab_cd(tp->seltab, "~"))
        weprintf("cd %s failed.", "~");
    tp_print(tp);
}
void
cmd_jumpmark(unsigned int pre, key *post, const Arg *arg, void *v)
{
    tabpane *tp = v;
    if (!post[0].utfk)
        return;
    char *path = getmark(post[0].ku.r);
    if (!path)
        return;
    tab_cd(tp->seltab, path);
    tp_print(tp);
}
void
cmd_setmark(unsigned int pre, key *post, const Arg *arg, void *v)
{
    tabpane *tp = v;
    if (!post[0].utfk)
        return;
    setmark(post[0].ku.r, tp->seltab->dv->dir);
}
void
cmd_togglesel(unsigned int pre, key *post, const Arg *arg, void *v)
{
    tabpane *tp = v;
    dv_toggleselect_item(tp->seltab->dv, tp->seltab->dv->cur.n);
    tab_move_rel(tp->seltab, 1);
    tp_print(tp);
}

static void
open_shell(char* cwd)
{
    if (chdir(cwd) < 0)
        return;
    char *shell = getenv("SHELL");
    if(!shell)
        shell = "/bin/sh";
    char* argv[] = { basename(shell), NULL };
    execfw(shell, argv, 0, 0, 0, 0, 1);
}

void
cmd_open_shell(unsigned int pre, key *post, const Arg *arg, void *v)
{
    tabpane *tp = v;
    char* cwd = tp->seltab->dv->dir;
    if(!cwd)
        return;
    open_shell(cwd);
}


void
cmd_list_tar(unsigned int pre, key *post, const Arg *arg, void *v)
{
    tabpane *tp = v;
    char *buf;
    char *dir = tp->seltab->dv->dir;
    char *cur = tp->seltab->dv->cur.name;
    if (!dir || !cur || chdir(dir))
        return;
    buf = pathmcat(dir, cur);
    char* argv[] = { "tar", "tf", buf, NULL };
    execfw(argv[0], argv, 0, 1, 1, 0, 0);
    free(buf);
}

void
cmd_edit(unsigned int pre, key *post, const Arg *arg, void *v)
{
    tabpane *tp = v;
    char *buf;
    char *dir = tp->seltab->dv->dir;
    char *cur = tp->seltab->dv->cur.name;
    if (!dir || !cur || chdir(dir))
        return;
    buf = pathmcat(dir, cur);
    char *editor = getenv("EDITOR");
    if (!editor)
        editor = "vi";
    char* argv[] = { editor, buf, NULL };
    execfw(argv[0] , argv, 0, 0, 1, 0, 0);
    free(buf);
}

void
cmd_search(unsigned int pre, key *post, const Arg *arg, void *v)
{
    vcursctx_mode_switch(ctx, mode_search);
}

void
cmd_search_next(unsigned int pre, key *post, const Arg *arg, void *v)
{
    tabpane *tp = v;
    tab_search_next(tp->seltab, NULL);
    tp_print(tp);
}
void
cmd_search_prev(unsigned int pre, key *post, const Arg *arg, void *v)
{
    tabpane *tp = v;
    tab_search_prev(tp->seltab, NULL);
    tp_print(tp);
}

void
cmd_tab_open(unsigned int pre, key *post, const Arg *arg, void *v)
{
    tabpane *tp = v;
    if ((char *)arg->v)
        tp_tab_open(tp, "/", (char *)arg->v);
    else
        tp_tab_open(tp, "/", tp->seltab->dv->dir);
    tp_print(tp);
}
void
cmd_tab_close(unsigned int pre, key *post, const Arg *arg, void *v)
{
    tabpane *tp = v;
    tp_tab_close(tp, tp->nsel);
    if(master && !master->tabs_size)
    {
        tp_del(master);
        master = NULL;
        if(!slave)
            cleanup_exit(0);
        master = slave;
        slave = NULL;
        seltp = master;
        dualpane = 0;
        layout_win();
    }
    if(slave && !slave->tabs_size)
    {
        tp_del(slave);
        slave = NULL;
        seltp = master;
        dualpane = 0;
        layout_win();
    }
    tp_print(seltp);
}
void
cmd_tab_prev(unsigned int pre, key *post, const Arg *arg, void *v)
{
    tabpane *tp = v;
    if (!pre)
        tp_sel_tab_rel(tp, -1);
    else
        tp_sel_tab_rel(tp, -pre);
    tp_print(tp);
}
void
cmd_tab_next(unsigned int pre, key *post, const Arg *arg, void *v)
{
    tabpane *tp = v;
    if (!pre)
        tp_sel_tab_rel(tp, 1);
    else
        tp_sel_tab(tp, pre - 1);
    tp_print(tp);
}
void
cmd_toggle_dualpane(unsigned int pre, key *post, const Arg *arg, void *v)
{
    if(dualpane)
    {
        tp_del(slave);
        slave = NULL;
        seltp = master;
    }
    dualpane = !dualpane;
    layout_win();
    print_all();
}
void
cmd_toggle_selpane(unsigned int pre, key *post, const Arg *arg, void *v)
{
    if (!dualpane)
        return;
    seltp = seltp == master ? slave : master;
    print_fill();
}
void
cmd_order(unsigned int pre, key *post, const Arg *arg, void *v)
{
    tabpane *tp = v;
    sort s = tp->seltab->dv->st;
    s.type = ((int*)arg->v)[0];
    s.reverse = ((int*)arg->v)[1];
    dv_sort(tp->seltab->dv, s);
    tp_print(tp);
}
void
cmd_order_toggle_reverse(unsigned int pre, key *post, const Arg *arg, void *v)
{
    tabpane *tp = v;
    sort s = tp->seltab->dv->st;
    s.reverse = !s.reverse;
    dv_sort(tp->seltab->dv, s);
    tp_print(tp);
}
void
cmd_order_type(unsigned int pre, key *post, const Arg *arg, void *v)
{
    tabpane *tp = v;
    sort s = tp->seltab->dv->st;
    s.type = arg->i;
    dv_sort(tp->seltab->dv, s);
    tp_print(tp);
}
void
cmd_order_icase(unsigned int pre, key *post, const Arg *arg, void *v)
{
    tabpane *tp = v;
    sort s = tp->seltab->dv->st;
    s.icase = arg->i;
    dv_sort(tp->seltab->dv, s);
    tp_print(tp);
}
void
cmd_order_reverse(unsigned int pre, key *post, const Arg *arg, void *v)
{
    tabpane *tp = v;
    sort s = tp->seltab->dv->st;
    s.reverse = arg->i;
    dv_sort(tp->seltab->dv, s);
    tp_print(tp);
}
void
cmd_order_dirs_first(unsigned int pre, key *post, const Arg *arg, void *v)
{
    tabpane *tp = v;
    sort s = tp->seltab->dv->st;
    s.dirs_first = arg->i;
    dv_sort(tp->seltab->dv, s);
    tp_print(tp);
}
void
cmd_yank_cur_str(tabpane *tp, char *s)
{
    int fd;
    execfw(yank_pipe_file, yank_pipe_arg, 1, 0, 1, &fd, 0);
    if (strlen(s) != write(fd, s, strlen(s)))
        weprintf("cmd_yank_cur_str: write error.");
    close(fd);
}
void
cmd_yank_cur_path(unsigned int pre, key *post, const Arg *arg, void *v)
{
    tabpane *tp = v;
    char *path = pathmcat(tp->seltab->dv->dir, tp->seltab->dv->cur.name);
    cmd_yank_cur_str(tp, path);
    free(path);
}
void
cmd_yank_cur_dir(unsigned int pre, key *post, const Arg *arg, void *v)
{
    tabpane *tp = v;
    cmd_yank_cur_str(tp, tp->seltab->dv->dir);
}
void
cmd_yank_cur_name(unsigned int pre, key *post, const Arg *arg, void *v)
{
    tabpane *tp = v;
    cmd_yank_cur_str(tp, tp->seltab->dv->cur.name);
}
void
cmd_write_dirfile(unsigned int pre, key *post, const Arg *arg, void *v)
{
    tabpane *tp = v;
    FILE *f;
    char *dirfile = getenv("RADIUM_DIRFILE");
    char *s = tp->seltab->dv->dir;
    if (!s) {
        weprintf("cmd_write_dirfile: dir is null.");
        return;
    }
    if (!dirfile) {
        weprintf("cmd_write_dirfile: env var RADIUM_DIRFILE not set.");
        return;
    }
    if (!(f = fopen(dirfile, "w"))) {
        weprintf("cmd_write_dirfile: fopen failed.");
        return;
    }
    fwrite(s, 1, strlen(s), f);
    if (ferror(f))
        weprintf("cmd_write_dirfile: fwrite failed.");
    if (fclose(f))
        weprintf("cmd_write_dirfile: flclose failed.");
}
void
cmd_H(unsigned int pre, key *post, const Arg *arg, void *v)
{
    tabpane *tp = v;
    if (tab_H_cd(tp->seltab, pre ? pre : 1))
        weprintf("tab_H_cd failed.");
    tp_print(tp);
}
void
cmd_L(unsigned int pre, key *post, const Arg *arg, void *v)
{
    tabpane *tp = v;
    if (tab_L_cd(tp->seltab, pre ? pre : 1))
        weprintf("tab_L_cd failed.");
    tp_print(tp);
}

/* Search / Cmd mode cmds*/
static void
mode_append_hist(mode *m, char *he)
{
    list_append((void **)&m->prompt_hist, &m->s_prompt_hist, &m->m_prompt_hist, sizeof(char *), &he);
    m->idx_hist_cur = m->s_prompt_hist;
}
static int
cmdmode_exec_prompt(void *v)
{
    if (!cmd_prompt->buf)
        return 0;
    int r = exec_cmd(cmd_prompt->buf, cmds, s_cmds, v);
    mode_append_hist(mode_cmd, strmcat("#", cmd_prompt->buf));
    prompt_freebuf(cmd_prompt);
    return r;
}
void
cmd_prompt_exec(unsigned int pre, key *post, const Arg *arg, void *v)
{
    cmdmode_exec_prompt(v);
    vcursctx_mode_switch(ctx, mode_normal);
}
void
cmd_switch_hist(unsigned int pre, key *post, const Arg *arg, void *v)
{
    mode *m = ctx->cur;
    if (!m || !(m == mode_cmd || m == mode_search))
        return;
    if (arg->i < 0)
        m->idx_hist_cur -= MIN(m->idx_hist_cur, -arg->i);
    else
        m->idx_hist_cur += MIN(m->s_prompt_hist - m->idx_hist_cur,  arg->i);
    char *he = 0;
    if (m->idx_hist_cur < m->s_prompt_hist)
        he = m->prompt_hist[m->idx_hist_cur];
    prompt_setstr(cmd_prompt, he);
    prompt_print(cmd_prompt);
}
void
cmd_prompt_search_submit(unsigned int pre, key *post, const Arg *arg, void *v)
{
    if (cmd_prompt->buf)
        mode_append_hist(mode_search, estrdup(cmd_prompt->buf));
    prompt_freebuf(cmd_prompt);
    vcursctx_mode_switch(ctx, mode_normal);
}
void
cmd_prompt_exit(unsigned int pre, key *post, const Arg *arg, void *v)
{
    vcursctx_mode_switch(ctx, mode_normal);
}
void
cmd_prompt_curs_left(unsigned int pre, key *post, const Arg *arg, void *v)
{
    prompt_curs_left(cmd_prompt);
    prompt_print(cmd_prompt);
}
void
cmd_prompt_curs_right(unsigned int pre, key *post, const Arg *arg, void *v)
{
    prompt_curs_right(cmd_prompt);
    prompt_print(cmd_prompt);
}
void
cmd_prompt_curs_start(unsigned int pre, key *post, const Arg *arg, void *v)
{
    prompt_curs_start(cmd_prompt);
    prompt_print(cmd_prompt);
}
void
cmd_prompt_curs_end(unsigned int pre, key *post, const Arg *arg, void *v)
{
    prompt_curs_end(cmd_prompt);
    prompt_print(cmd_prompt);
}
void
cmd_prompt_curs_bs(unsigned int pre, key *post, const Arg *arg, void *v)
{
    prompt_curs_bs(cmd_prompt);
    prompt_print(cmd_prompt);
}
void
cmd_prompt_curs_dc(unsigned int pre, key *post, const Arg *arg, void *v)
{
    prompt_curs_dc(cmd_prompt);
    prompt_print(cmd_prompt);
}
void
cmd_prompt_curs_dl(unsigned int pre, key *post, const Arg *arg, void *v)
{
    prompt_freebuf(cmd_prompt);
    prompt_print(cmd_prompt);
}
void
cmd_prompt_ins(unsigned int pre, key *post, const Arg *arg, void *v)
{
    if (!post[0].utfk)
        return;
    prompt_ins(cmd_prompt, post[0].ku.r);
    prompt_print(cmd_prompt);
}
void
cmd_prompt_complete(unsigned int pre, key *post, const Arg *arg, void *v)
{
    prompt_complete(cmd_prompt);
    prompt_print(cmd_prompt);
}
void
cmd_prompt_ins_digraph(unsigned int pre, key *post, const Arg *arg, void *v)
{
    if (!post[0].utfk || !post[1].utfk)
        return;
    if (post[0].ku.r < 0 || post[0].ku.r > 0x7F)
        return;
    if (post[1].ku.r < 0 || post[1].ku.r > 0x7F)
        return;
    Rune r = digraphtorune(post[0].ku.r, post[1].ku.r);
    if (r == Runeerror)
        return;
    prompt_ins(cmd_prompt, r);
    prompt_print(cmd_prompt);
}
