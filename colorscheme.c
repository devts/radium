#include <curses.h>

#include "colorscheme.h"

/* Default Colors
COLOR_BLACK
COLOR_RED
COLOR_GREEN
COLOR_YELLOW
COLOR_BLUE
COLOR_MAGENTA
COLOR_CYAN
COLOR_WHITE
Attributes
A_NORMAL
A_STANDOUT
A_UNDERLINE
A_REVERSE
A_BLINK
A_DIM
A_BOLD
A_PROTECT
A_INVIS
A_ALTCHARSET
A_CHARTEXT
COLOR_PAIR(n)
*/
static short nextpair = 3;
int getcolor(short fg, short bg)
{
    short pair = nextpair;
    nextpair++;
    init_pair(pair, fg, bg);
    return COLOR_PAIR(pair);
}

