vcursctx *ctx;

int dualpane;
tabpane *seltp;
tabpane *master;
tabpane *slave;
prompt *cmd_prompt;
void layout_win(void);
void cleanup_exit(int i);

void print_fill(void);
void print_all(void);

char** errors;
size_t s_errors;

enum y_status
{
    Y_NONE,
    Y_COPY,
    Y_CUT
};
int y_status;
char **y_sel;
size_t y_sels;
