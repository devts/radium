#include "all.h"

int
strendstr(char *a, char *b)
{
    size_t sa = strlen(a);
    size_t sb = strlen(b);
    if(sa < sb)
        return 0;
    a += sa - sb;
    return strncasecmp(a, b, sb) ? 0 : 1;
}

size_t
all_o_len(opener *o, dirview *dv)
{
    if(!dv)
        return 0;
    size_t ol = 0;
    for(size_t j = 0; j < dv->dirlist_size; j++)
    {
        char *file = dv->dirlist[j].name;
        for(size_t i = 0; i < rules_len; i++)
        {
            if(strendstr(file, rules[i].ext))
            {
                if(rules[i].o == o)
                    ol++;
                break;
            }
        }
    }
    return ol;
}
void
all_o(char **argv, opener *o, dirview *dv)
{
    if(!dv)
        return;
    size_t ol = 0;
    for(size_t j = 0; j < dv->dirlist_size; j++)
    {
        char *file = dv->dirlist[j].name;
        for(size_t i = 0; i < rules_len; i++)
        {
            if(strendstr(file, rules[i].ext))
            {
                if(rules[i].o == o)
                    argv[ol++] = file;
                break;
            }
        }
    }
}
int
open_file(char *file, dirview *dv)
{
    for(size_t i = 0; i < rules_len; i++)
    {
        if(strendstr(file, rules[i].ext))
        {
            size_t argc = rules[i].o->argc;
            char** argv = ecalloc(argc + 1 + (rules[i].o->append_all ? all_o_len(rules[i].o, dv) : 0), sizeof(char *));
            for(size_t j = 0; j < argc; j++)
                argv[j] = rules[i].o->argv[j] ? rules[i].o->argv[j] : file;
            if(rules[i].o->append_all)
                all_o(argv + argc, rules[i].o, dv);
            execfw(rules[i].o->file, argv, rules[i].o->fork, 0, 1, 0, 0);
            free(argv);
            return 0;
        }
    }
    return 1;
}
int
open_file_with(char *ex, size_t argc, char **argv, char *file)
{
    char** argv2 = ecalloc(argc + 1, sizeof(char *));
    for(size_t j = 0; j < argc; j++)
        argv2[j] = argv[j] ? argv[j] : file;
    execfw(ex, argv2, 1, 0, 1, 0, 0);
    free(argv2);
    return 0;
}
