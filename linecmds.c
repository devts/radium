#include "all.h"

static int
f_delete(char *path, fileinfo *fi, void *v)
{
    if (fs_rm(path))
        return weprintf("rm %s failed.", path), 1;
    return 0;
}
void
cmd_delete(size_t argc, char** argv, char *cmd, void *v)
{
    tabpane *tp = v;
    if(argc != 0) {
        weprintf("invalid argument count.");
        return;
    }
    dv_iterate_as(tp->seltab->dv, f_delete, 0);
    dv_update(tp->seltab->dv, 1);
}
void
cmd_rename(size_t argc, char** argv, char *cmd, void *v)
{
    tabpane *tp = v;
    tab *t = tp->seltab;
    if (argc != 1) {
        weprintf("invalid argument count.");
        return;
    }
    if (!*(argv[0])) {
        weprintf("invalid argument.");
        return;
    }
    char *src, *dst;
    src = pathmcat(t->dv->dir, t->dv->cur.name);
    dst = pathmcat(t->dv->dir, argv[0]);
    if (fs_rename(src, dst))
        weprintf("rename %s -> %s failed.", src, dst);
    free(src);
    free(dst);
    dv_update(t->dv, 1);
}
static int
f_rr(char *path, fileinfo *fi, void *v)
{
    int ret = 1;
    char *p1 = estrdup(path);
    char *p2 = estrdup(path);
    char *dir = dirname(p1);
    char *name = basename(p2);
    char *dname = subst((char *)v, name);
    char *dst = 0;
    if (!dname) {
        weprintf("substitution %s failed.", (char *)v);
        goto f_rr_free;
    }
    dst = pathmcat(dir, dname);
    if (!strcmp(path, dst)) {
        ret = 0;
        goto f_rr_free;
    }
    if (fs_rename(path, dst)) {
        weprintf("rename %s failed.", path);
        goto f_rr_free;
    }
    ret = 0;
f_rr_free:
    free(p1);
    free(p2);
    free(dname);
    if (dst)
        free(dst);
    return ret;
}
void
cmd_regex_rename(size_t argc, char** argv, char *cmd, void *v)
{
    tabpane *tp = v;
    if(argc != 1) {
        weprintf("invalid argument count.");
        return;
    }
    dv_iterate_as(tp->seltab->dv, f_rr, argv[0]);
    dv_update(tp->seltab->dv, 1);
}
void
cmd_touch(size_t argc, char** argv, char *cmd, void *v)
{
    tabpane *tp = v;
    char *buf;
    tab *t = tp->seltab;
    if (argc != 1) {
        weprintf("invalid argument count.");
        return;
    }
    if (!(buf = canonic_path(t->dv->dir, argv[0])))
        return;
    size_t l = strlen(buf);
    if (l && buf[l - 1] == '/')
        buf[l - 1] = 0;
    if (fs_touch(buf))
        weprintf("touch %s failed.", buf);
    free(buf);
    dv_update(t->dv, 1);
}
void
cmd_mkdir(size_t argc, char** argv, char *cmd, void *v)
{
    tabpane *tp = v;
    char *buf;
    tab *t = tp->seltab;
    if (argc != 1) {
        weprintf("invalid argument count.");
        return;
    }
    if (!(buf = canonic_path(t->dv->dir, argv[0])))
        return;
    if (fs_mkdir(buf))
        weprintf("mkdir %s failed.", buf);
    free(buf);
    dv_update(t->dv, 1);
}
void
cmd_cd(size_t argc, char** argv, char *cmd, void *v)
{
    tabpane *tp = v;
    if (argc > 1) {
        weprintf("invalid argument count.");
        return;
    }
    char *path = argc ? argv[0] : "";
    if (tab_cd(tp->seltab, path))
        weprintf("cd %s failed.", path);
}
void
cmd_regsel(size_t argc, char** argv, char *cmd, void *v)
{
    tabpane *tp = v;
    if(argc != 1) {
        weprintf("invalid argument count.");
        return;
    }
    dv_regsel(tp->seltab->dv, argv[0]);
}
void
cmd_clearsel(size_t argc, char** argv, char *cmd, void *v)
{
    tabpane *tp = v;
    if(argc != 0) {
        weprintf("invalid argument count.");
        return;
    }
    dv_empty_selection(tp->seltab->dv);
}
static int
print_as_item(char *path, fileinfo *fi, void *v)
{
    FILE *fp = *(FILE **)v;
    fwrite(path, 1, strlen(path) + 1, fp);
    return ferror(fp);
}
void
cmd_writesel(size_t argc, char** argv, char *cmd, void *v)
{
    tabpane *tp = v;
    FILE *fp;
    char *buf;
    size_t l;
    if (argc > 1) {
        weprintf("invalid argument count.");
        return;
    }
    if (argc) {
        if (!(buf = canonic_path(tp->seltab->dv->dir, argv[0])))
            return;
        l = strlen(buf);
        if (l && buf[l - 1] == '/')
            buf[l - 1] = 0;
        fp = fopen(buf, "w");
        free(buf);
    } else {
        fp = fopen("/tmp/rasel", "w");
    }
    if (!fp)
        return;
    if (dv_iterate_as(tp->seltab->dv, print_as_item, &fp))
        weprintf("error writing selection to file.");
    fclose(fp);
}
void
cmd_open_with(size_t argc, char** argv, char *cmd, void *v)
{
    tabpane *tp = v;
    tab *t = tp->seltab;
    char *dir = t->dv->dir;

    if (!dir || chdir(dir))
        return;
    if(!argc) {
        weprintf("invalid argument count.");
        return;
    }
    size_t argc2 = argc + 1;
    char** argv2 = ecalloc(argc + 1, sizeof(char *));
    for(size_t j = 0; j < argc; j++)
        argv2[j] = strcmp(argv[j], "%") ? argv[j] : (argc2 = argc, NULL);
    open_file_with(argv[0], argc2, argv2, t->dv->cur.name);
    free(argv2);
}
