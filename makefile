EXEC=radium
DBGEXEC=radiumdbg

all: $(EXEC)
debug: $(EXEC) $(DBGEXEC)
perf: $(EXEC)
srelease: $(EXEC)

FILES =\
	colorscheme.c dirview.c radium.c tab.c tabpane.c \
	fileopener.c config.c linecmds.c keycmds.c marks.c modes.c \
	libvcurs/keyhandler.c libvcurs/prompt.c libvcurs/cmdline.c libvcurs/mode.c \
	libutil/ealloc.c libutil/estrdup.c libutil/strmcat.c libutil/mpath.c \
	libutil/eprintf.c libutil/strcasestr.c libutil/strlcat.c libutil/strlcpy.c \
	libutil/list_functions.c libutil/nstimer.c libutil/subst.c libutil/execfw.c \
	libutf/utf.c libutf/rune.c libutf/digraph.c \
	libfs/mode.c libfs/fsm.c libfs/fsort.c libfs/fsutil.c libfs/path.c 

DESTDIR ?= $(HOME)
CPUTYPE ?= amd64
PREFIX ?= /bin/$(CPUTYPE)
MANPREFIX ?= /man

CC ?= cc
CPPFLAGS ?= -D_XOPEN_SOURCE=700
COMMONCFLAGS ?= -std=c99 -Wall -pedantic -Werror-implicit-function-declaration -fcommon
CFLAGS ?= $(CPPFLAGS) $(COMMONCFLAGS) -O2
LIBS ?= -lcurses
LDFLAGS ?= -s $(LIBS)

DBGCFLAGS ?= $(CPPFLAGS) $(COMMONCFLAGS) -g -O0
DBGLDFLAGS ?= $(LIBS)

srelease: CC = musl-gcc
srelease: CFLAGS = -I../ncurses/include -L../ncurses/lib $(CPPFLAGS) $(COMMONCFLAGS) -O2
srelease: LDFLAGS = -static -s $(LIBS)
perf: CFLAGS += -pg
perf: LDFLAGS = $(LIBS)

config.c:
	@echo creating config.c from config.def.c
	cp config.def.c config.c

$(EXEC): $(FILES)
	$(CC) $(CFLAGS) $(FILES) $(LDFLAGS) -o $@

$(DBGEXEC): $(FILES)
	$(CC) $(DBGCFLAGS) $(FILES) $(DBGLDFLAGS) -o $@

clean:
	rm -f $(EXEC) $(DBGEXEC)

install:
	mkdir -p $(DESTDIR)$(PREFIX)
	cp -f $(EXEC) $(DESTDIR)$(PREFIX)/$(EXEC)
	mkdir -p $(DESTDIR)$(MANPREFIX)/man1
	cp -f $(EXEC).1 $(DESTDIR)$(MANPREFIX)/man1/$(EXEC).1

.PHONY: clean install debug perf srelease
