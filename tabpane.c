#include "all.h"

tabpane *tp_new(Wspec *w, char *cwd, char *arg)
{
    tabpane *tp = ecalloc(1, sizeof(tabpane)); 
    tp->w = w;
    tp_layout_win(tp);
    if (tp_tab_open(tp, cwd, arg)) {
        tp_del(tp);
        return NULL;
    }
    return tp;
}
void tp_del(tabpane *tp)
{
    if (!tp)
        return;
    tp->w = NULL;
    if (tp->tabbar)
        delwin(tp->tabbar);
    tp->tabbar = NULL;
    if (tp->tabw)
        delwin(tp->tabw);
    tp->tabw = NULL;
    tp->seltab = NULL;
    for (size_t i = 0; i < tp->tabs_size; i++)
        tab_del(tp->tabs[i]);
    free(tp->tabs);
    tp->tabs = NULL;
    free(tp);
}

static char tabbar_sep[] = " ";
void
tp_print_tabbar(tabpane *tp)
{
    size_t n;
    int mrow,mcol;
    tab *t;

    if (!tp->tabw || !tp->tabs_size || !tp->seltab)
        return;
    if (ra_otnb && tp->tabs_size < 2)
        return;

    getmaxyx(tp->tabbar,mrow,mcol);
    werase(tp->tabbar);
    if (!mrow)
        return;
    n = 0;
    for (size_t i = 0; i < tp->tabs_size; i++) {
        t = tp->tabs[i];
        char *cwd = estrdup(t->dv->dir); /*basename will remove trailing slash, so we need a copy*/
        char *name = basename(cwd);
        wattrset(tp->tabbar, t == tp->seltab ? scheme.TAB_ACTIVE_ATTR : scheme.TAB_INACTIVE_ATTR);
        mvwaddnstr(tp->tabbar, 0, n, name, mcol - n);
        n += strlen(name);
        mvwaddnstr(tp->tabbar, 0, n, tabbar_sep, mcol - n);
        n += sizeof(tabbar_sep);
        free(cwd);
    }
    wrefresh(tp->tabbar);
}

void
tp_print(tabpane *tp)
{
    int mrow, mcol;
    getmaxyx(tp->tabw, mrow, mcol);
    tp_print_tabbar(tp);
    tp->seltab->mrow = mrow;
    tp->seltab->mcol = mcol;
    tab_bring_cur_in_view(tp->seltab);
    tab_print(tp->seltab, tp->tabw);
}

int
tp_update(tabpane *tp)
{
    int r;
    if ((r = dv_update(tp->seltab->dv, 0)) == -1)
        return r;
    tp_print(tp);
    return r;
}

void
tp_layout_win(tabpane *tp)
{
    delwin(tp->tabbar);
    delwin(tp->tabw);
    tp->tabbar = subwin(stdscr, 1, tp->w->mcol, tp->w->row, tp->w->col);
    if (ra_otnb && tp->tabs_size < 2)
        tp->tabw = subwin(stdscr, tp->w->mrow, tp->w->mcol, tp->w->row, tp->w->col);
    else
        tp->tabw = subwin(stdscr, tp->w->mrow - 1, tp->w->mcol, tp->w->row + 1, tp->w->col);
}
int
tp_tab_open(tabpane *tp, char *cwd, char *arg)
{
    tab *new = tab_new(cwd, arg);
    if (!new)
        return 1;
    tp->tabs = ereallocarray(tp->tabs, ++(tp->tabs_size), sizeof(tab *));
    tp->tabs[tp->tabs_size - 1] = new;
    tp->seltab = new;
    tp->nsel = tp->tabs_size - 1;
    if (ra_otnb && tp->tabs_size == 2)
        tp_layout_win(tp);
    return 0;
}

int
tp_tab_close(tabpane *tp, size_t n)
{
    if (n >= tp->tabs_size)
        return 1;
    tab_del(tp->tabs[n]);
    tp->tabs[n] = 0;
    tp->tabs_size--;
    if (!tp->tabs_size) {
        tp->seltab = NULL;
    } else if (n == tp->tabs_size) {
        if (n == tp->nsel) {
            tp->nsel = n - 1;
            tp->seltab = tp->tabs[n - 1];
        }
    } else {
        memmove(tp->tabs + n, tp->tabs + n + 1, (tp->tabs_size - n) * sizeof(tab *));
        if (n == tp->nsel)
            tp->seltab = tp->tabs[n];
        else if (n < tp->nsel)
            tp->nsel--;
    }
    if (ra_otnb && tp->tabs_size == 1)
        tp_layout_win(tp);
    return 0;
}

int
tp_sel_tab(tabpane *tp, size_t n)
{
    if (n >= tp->tabs_size)
        return 1;
    tp->nsel = n;
    tp->seltab = tp->tabs[n];
    return 0;
}
int
tp_sel_tab_rel(tabpane *tp, int n)
{
    while (n < 0)
        n+= tp->tabs_size;
    return tp_sel_tab(tp, (tp->nsel + n) % tp->tabs_size);
}
