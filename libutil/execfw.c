#include "../all.h"

void
execfw(char *file, char *argv[], int detach, int wait, int p, int *fdp, int fg)
{
    int status, fd;
    int fildes[2];
    if (fdp && !detach)
        return;
    if (fdp && pipe(fildes) < 0)
        return;
    if (fdp)
        *fdp = fildes[1];
    if (!detach)
        endwin();
    pid_t ret;
    pid_t pid = fork();
    if (pid == -1) 
        return;
    if (pid) 
    {
        if (fdp)
            close(fildes[0]);
        if (!detach)
        {
            while ((ret = waitpid(pid, &status, 0)) == -1) 
            {
                if (errno != EINTR) 
                    break;
            }
        }
    } 
    else 
    {
        if (detach)
        {
            fd = open("/dev/null", O_RDWR);
            if (fdp) {
                close(fildes[1]);
                dup2(fildes[0], STDIN_FILENO);
            } else {
                if (fd < 0)
                    close(STDIN_FILENO);
                else
                    dup2(fd, STDIN_FILENO);
            }
            if (fd < 0) {
                close(STDOUT_FILENO);
                close(STDERR_FILENO);
            } else {
                dup2(fd, STDOUT_FILENO);
                dup2(fd, STDERR_FILENO);
                close(fd);
            }
            setsid();
        }
        if (fg) {
            pid_t chld = getpid();
            setpgid(chld, chld);
            tcsetpgrp(0, chld);
        }
        signal(SIGCHLD, SIG_DFL);
        (p ? execvp : execv)(file, argv);
        _Exit(127);
    }
    if (fg)
        tcsetpgrp(0, getpgid(getpid()));
    if (!detach && wait)
    {
        puts("Press a key to continue.");
        getchar();
    }
    if (!detach)
        refresh();
}
