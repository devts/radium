#undef MIN
#define MIN(x,y)  ((x) < (y) ? (x) : (y))
#undef MAX
#define MAX(x,y)  ((x) > (y) ? (x) : (y))
#undef LIMIT
#define LIMIT(x, a, b)  (x) = (x) < (a) ? (a) : (x) > (b) ? (b) : (x)
#undef CLAMP
#define CLAMP(x, a, b)  ((x) < (a) ? (a) : (x) > (b) ? (b) : (x))
#define NUMCMP(a,b)  ((a) > (b) ? 1 : ((a) < (b) ? -1 : 0 ))
#define ISRANGE(x, a, b) (((x) >= (a)) && ((x) <= (b)))
#define LEN(x) (sizeof (x) / sizeof *(x))
#ifndef EPSILON
#define EPSILON 0.00001
#endif
#define EQ_EPS(x, y, e) (fabs((x)-(y)) < (e))
#define NEAR_F(x, y) (EQ_EPS((x),(y),EPSILON))
#define SQ(x) ((x)*(x))

/* ealloc.c */
void *ecalloc(size_t, size_t);
void *emalloc(size_t);
void *erealloc(void *, size_t);
#undef reallocarray
void *reallocarray(void *, size_t, size_t);
void *ereallocarray(void *, size_t, size_t);

/* estrdup.c */
char *estrdup(const char *);
char *estrndup(const char *, size_t);

/* strmcat.c */
char *strmcat(const char *s, const char *t);
char *strmcat3(const char *s, const char *t, const char *u);

/* mpath.c */
char *pathmcat(const char *s, const char *t);
char *mdirname(const char *path);
char *mbasename(const char *path);
char *mgetcwd(void);

/* strlcat.c */
#undef strlcat
size_t strlcat(char *, const char *, size_t);
int rstrlcat(char *, const char *, size_t);
/* strlcpy.c */
#undef strlcpy
size_t strlcpy(char *, const char *, size_t);
int rstrlcpy(char *, const char *, size_t);

/* strcasestr.c */
#undef strcasestr
char *strcasestr(const char *, const char *);

/* subst.c */
char *subst(char *cmd, char *str);

/* eprintf.c */
void enprintf(int, const char *, ...);
void weprintf(const char *, ...);

/* execfw.c */
void execfw(char *file, char *argv[], int detach, int wait, int p, int *fdp, int fg);


/* list_functions.c */
void list_minsize(void **l, size_t *len, size_t *msize, size_t itemsize, size_t min);
void list_append(void **l, size_t *len, size_t *msize, size_t itemsize, void *item);
void list_append_range(void **l, size_t *len, size_t *msize, size_t itemsize, size_t numitems, void *items);
void list_find(void **l, size_t *len, size_t *msize, size_t itemsize, void *item, char **v, size_t *i);
void list_remove(void **l, size_t *len, size_t *msize, size_t itemsize, void *item);
void list_remove_idx(void **l, size_t *len, size_t *msize, size_t itemsize, size_t idx);
void list_remove_idx_range(void **l, size_t *len, size_t *msize, size_t itemsize, size_t idx, size_t num);
void list_insert_at(void **l, size_t *len, size_t *msize, size_t itemsize, size_t idx, void *item);
void list_insert_range_at(void **l, size_t *len, size_t *msize, size_t itemsize, size_t idx, size_t numitems, void *items);
void list_malloc_trim(void **l, size_t *len, size_t *msize, size_t itemsize);

/* nstimer.c */
typedef struct {
  struct timespec start;
  uint64_t t_ns;
} nstimer;
nstimer *nstimer_new(void);
void nstimer_start(nstimer *t);
void nstimer_continue(nstimer *t);
void nstimer_stop(nstimer *t);
void nstimer_delete(nstimer *t);
