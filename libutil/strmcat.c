#include "../all.h"

char *
strmcat(const char *s, const char *t)
{
    size_t ss = strlen(s);
    size_t st = strlen(t);
    char *p = emalloc(ss + st + 1);
    memcpy(p, s, ss);
    memcpy(p + ss, t, st);
    p[ss + st] = 0;
    return p;
}

char *
strmcat3(const char *s, const char *t, const char *u)
{
    size_t ss = strlen(s);
    size_t st = strlen(t);
    size_t su = strlen(u);
    char *p = emalloc(ss + st + su + 1);
    memcpy(p, s, ss);
    memcpy(p + ss, t, st);
    memcpy(p + ss + st, u, su);
    p[ss + st + su] = 0;
    return p;
}
