#include "../all.h"

keyhandler *
keyhandler_new(modebinds *m) {
    keyhandler *kh = ecalloc(1, sizeof(keyhandler));
    kh->m = m;
    keyhandler_reset(kh);
    list_minsize((void **)&kh->kb, &kh->s_kb, &kh->m_kb, sizeof(key), 255);
    list_minsize((void **)&kh->post, &kh->s_post, &kh->m_post, sizeof(key), 255);
    return kh;
}

void
keyhandler_delete(keyhandler *kh) {
    free(kh->kb);
    free(kh->post);
    free(kh);
}

void
keyhandler_reset(keyhandler *kh) {
    kh->num_uch = 0;
    kh->s_kb = 0;
    kh->s_post = 0;
    kh->pre = 0;
    kh->is_pre = 1;
    kh->is_post = 0;
}

void
keyhandler_switch_mode(keyhandler *kh, modebinds *m) {
    keyhandler_reset(kh);
    kh->m = m;
}

int
key_cmp(key *a, key *b) {
    return (a->utfk != b->utfk) || (a->utfk ? a->ku.r != b->ku.r : a->ku.k != b->ku.k);
}

int
keyhandler_handle_curses_key(keyhandler *kh, int ck, void *v) {
    key k;
    if (!kh->m) {
        weprintf("this mode has no bindings");
        return 1;
    }
    if (ck > 0xFF) {
        if (kh->num_uch) {
            keyhandler_reset(kh);
            weprintf("unfinished unicode char before keycode");
            return 1;
        }
        k.utfk = 0;
        k.ku.k = ck;
    } else {
        char ch = ck;
        if (kh->num_uch >= UTFmax) {
            keyhandler_reset(kh);
            weprintf("invalid utf8");
            return 1;
        }
        kh->uch[kh->num_uch++] = ch;
        Rune r = Runeerror;
        if (!charntorune(&r, kh->uch, kh->num_uch))
            return 0;
        if (r == Runeerror) {
            keyhandler_reset(kh);
            weprintf("invalid utf8");
            return 1;
        }
        k.utfk = 1;
        k.ku.r = r;
        kh->num_uch = 0;
    }
    if (kh->m->do_pre && kh->is_pre) {
        if (k.utfk && k.ku.r >= (Rune)'0' && k.ku.r <= (Rune)'9') {
            kh->pre = kh->pre*10 + (k.ku.r - (Rune)'0');
            return 0;
        } else {
            kh->is_pre = 0;
        }
    }
    if (!kh->is_post)
        list_append((void **)&kh->kb, &kh->s_kb, &kh->m_kb, sizeof(key), &k);
    else
        list_append((void **)&kh->post, &kh->s_post, &kh->m_post, sizeof(key), &k);

    int match_possible = 0; /* input of further keys could yield a match */
    for(size_t i = 0; i < kh->m->s_kbs; i++) {
        if (kh->m->kbs[i].s_keys < kh->s_kb)
            continue;
        int this_match_possible = 1;
        for (size_t j = 0; j < kh->s_kb; j++) {
            if (key_cmp(&kh->kb[j], &kh->m->kbs[i].keys[j])) {
                this_match_possible = 0;
                break;
            }
        }
        match_possible |= this_match_possible;
        if (this_match_possible && kh->m->kbs[i].s_keys == kh->s_kb) { /*command kbs[i] was typed*/
            if (kh->s_post < kh->m->kbs[i].s_post) {
                kh->is_post = 1;
                return 0;
            }
            if (kh->m->kbs[i].func)
                kh->m->kbs[i].func(kh->pre, (kh->m->kbs[i].s_post ? kh->post : 0), &(kh->m->kbs[i].arg), v);
            keyhandler_reset(kh);
            return 0;
        }
    }
    if (match_possible)
        return 0;
    if (!kh->m->fallthr) {
        keyhandler_reset(kh);
        if (!(k.utfk && k.ku.r == 27))
            weprintf("no matching keybind");
        return 1;
    }
    if (kh->m->fallthr->func)
        kh->m->fallthr->func(kh->pre, kh->kb, &(kh->m->fallthr->arg), v);
    keyhandler_reset(kh);
    return 0;
}
