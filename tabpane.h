typedef struct
{
    int row;
    int col;
    int mrow;
    int mcol;
} Wspec;

typedef struct
{
    Wspec *w;
    WINDOW *tabbar;
    WINDOW *tabw;

    tab** tabs;
    size_t tabs_size;
    size_t nsel;
    tab* seltab;
} tabpane;

tabpane *tp_new(Wspec *w, char *cwd, char *arg);
void tp_del(tabpane *tp);
void tp_layout_win(tabpane *tp);
void tp_print_tabbar(tabpane *tp);
void tp_print(tabpane *tp);
int tp_update(tabpane *tp);
int tp_tab_open(tabpane *tp, char *cwd, char *arg);
int tp_tab_close(tabpane *tp, size_t n);
int tp_sel_tab(tabpane *tp, size_t n);
int tp_sel_tab_rel(tabpane *tp, int n);
