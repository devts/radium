typedef struct colorscheme colorscheme;
struct colorscheme
{
    int DEFAULT_ATTR;
    int ERROR_ATTR;
    int TAB_INACTIVE_ATTR;
    int TAB_ACTIVE_ATTR;
    int CWD_ATTR;
    int MODE_ATTR;
    int DIR_ATTR;
    int FILE_ATTR;
    int SUID_ATTR;
    int EXECUTABLE_ATTR;
    int LNK_ATTR;
    int FIFO_ATTR;
    int SOCKET_ATTR;
    int DEVICE_ATTR;
    int CUR_OR_ATTR;
    int SELECTED_ATTR;
};
colorscheme scheme;
int getcolor(short fg, short bg);
