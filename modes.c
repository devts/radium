#include "all.h"
mode *mode_normal = 0;
mode *mode_cmd = 0;
mode *mode_search = 0;

void
setcmdline(char prefix,
        void (*fstrchg)(char *, size_t, void *), void* argf,
        void (*complete)(char *, size_t, size_t, void *), void *argc) 
{
    cmd_prompt->attr = scheme.DEFAULT_ATTR;
    cmd_prompt->prefix = prefix;
    cmd_prompt->fstrchg = fstrchg;
    cmd_prompt->argf = argf;
    cmd_prompt->complete = complete;
    cmd_prompt->argc = argc;
    cmd_prompt->visible = 1;
    prompt_freebuf(cmd_prompt);
    curs_set(1);
    prompt_print(cmd_prompt);
}

void
unsetcmdline(void) {
    curs_set(0);
    cmd_prompt->prefix = ' ';
    cmd_prompt->fstrchg = 0;
    cmd_prompt->argf = 0;
    cmd_prompt->complete = 0;
    cmd_prompt->argc = 0;
    cmd_prompt->visible = 0;
    prompt_freebuf(cmd_prompt);
}

void
mode_normal_start(vcursctx *ctx, mode *m) {
    print_all();
}

void
mode_normal_end(vcursctx *ctx, mode *m) {
}

void
mode_cmd_start(vcursctx *ctx, mode *m) {
    setcmdline(':', 0, 0, 0, 0);
}

void
mode_cmd_end(vcursctx *ctx, mode *m) {
    unsetcmdline();
}

static void
searchnext(char *buf, size_t s_buf, void *v)
{
    tabpane *tp = v;
    tab_search_next(tp->seltab, buf);
    tp_print(tp);
}

void
mode_search_start(vcursctx *ctx, mode *m) {
    tabpane *tp = ctx->v;
    setcmdline('/', searchnext, tp, 0, 0);
}

void
mode_search_end(vcursctx *ctx, mode *m) {
    unsetcmdline();
}

void
modes_init(void) {
    modebinds *nbinds = emalloc(sizeof(modebinds));
    *nbinds = (modebinds){normal_kbs, s_normal_kbs, normal_fallthr, 1};
    mode_normal = mode_new(0, 0, 0, 0, nbinds, mode_normal_start, mode_normal_end);

    modebinds *cbinds = emalloc(sizeof(modebinds));
    *cbinds = (modebinds){cmd_kbs, s_cmd_kbs, cmd_fallthr, 0};
    mode_cmd = mode_new(0, 0, 0, 0, cbinds, mode_cmd_start, mode_cmd_end);

    modebinds *sbinds = emalloc(sizeof(modebinds));
    *sbinds = (modebinds){search_kbs, s_search_kbs, search_fallthr, 0};
    mode_search = mode_new(0, 0, 0, 0, sbinds, mode_search_start, mode_search_end);
}

void modes_free(void) {
    if (mode_normal) {
        free(mode_normal->mb);
        mode_delete(mode_normal);
    }
    if (mode_cmd) {
        free(mode_cmd->mb);
        mode_delete(mode_cmd);
    }
    if (mode_search) {
        free(mode_search->mb);
        mode_delete(mode_search);
    }
}
