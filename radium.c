#include "all.h"

vcursctx *ctx = 0;
int dualpane = 0;
tabpane *seltp = NULL;
tabpane *master = NULL;
tabpane *slave = NULL;
prompt *cmd_prompt = 0;
WINDOW *fill = NULL;

static char *new_cwd = NULL;
static char *cwd = NULL;

static Wspec mwin, swin;
void
layout_win(void)
{
    int mrow,mcol;
    getmaxyx(stdscr,mrow,mcol);
    if (cmd_prompt)
        delwin(cmd_prompt->w);
    else
        cmd_prompt = prompt_new(0, scheme.DEFAULT_ATTR, ' ', 0, 0, 0, 0);
    cmd_prompt->w = subwin(stdscr, 1, mcol, mrow - 1, 0);

    mwin.mrow = mrow;
    mwin.mcol = dualpane ? mcol/2 : mcol;
    mwin.row = 0;
    mwin.col = 0;
    if (!master) {
        master = tp_new(&mwin, cwd, new_cwd);
    } else {
        master->w = &mwin;
        tp_layout_win(master);
    }
    if (dualpane) {
        swin.mrow = mrow;
        swin.mcol = mcol - mcol/2 - 1;
        swin.row = 0;
        swin.col = mcol/2 + 1;
        if (!slave) {
            slave = tp_new(&swin, cwd, new_cwd);
        } else {
            slave->w = &swin;
            tp_layout_win(slave);
        }

        if (fill)
            delwin(fill);
        fill = subwin(stdscr, mrow, 1, 0, mcol/2);
    }
    if (!seltp)
        seltp = master;
}
void
print_fill(void)
{
    if(fill && dualpane)
    {
        werase(fill);
        mvwaddch(fill, 0, 0, seltp == master ? '<' : '>');
        wrefresh(fill);
    }
}
void
print_all(void)
{
    print_fill();
    if (master)
        tp_print(master);
    if (slave)
        tp_print(slave);
    if (cmd_prompt)
        prompt_print(cmd_prompt);
}
void
cleanup_exit(int i)
{
    keyhandler_delete(ctx->kh);
    ctx->kh = 0;
    ctx->cur = 0;
    ctx->v = 0;
    vcursctx_delete(ctx);
    modes_free();
    seltp = NULL;
    if(master)
        tp_del(master);
    if(slave)
        tp_del(slave);
    delwin(cmd_prompt->w);
    if(cmd_prompt)
        prompt_delete(cmd_prompt);
    if (cwd)
        free(cwd);
    endwin();
    exit(i);
}

#ifndef KEY_RESIZE
int resize = 0;
void
hresize(int i)
{
    resize = 1;
}
#endif
int ragetch(int err)
{
    int i;
    do
    {
        i = getch();
#ifdef KEY_RESIZE
        if (i==KEY_RESIZE) {
            layout_win();
            print_all();
        }
    } while(i == KEY_RESIZE || (!err && i == ERR));
#else
        if(resize)
        {
            endwin();
            layout_win();
            print_all();
            refresh();
            resize = 0;
        }
    } while(!err && i == ERR);
#endif
    return i;
}
char** errors = NULL;
size_t s_errors = 0;
int
main(int argc, char *argv[])
{
    setlocale(LC_ALL, "");
    if (argc > 2)
        enprintf(2, "Too many arguments\n");
    if (!(cwd = mgetcwd()))
        enprintf(1, "Error: getcwd(3) failed.\n");
    signal(SIGCHLD, SIG_IGN);
#ifndef KEY_RESIZE
    signal(SIGWINCH, hresize);
#endif
    initscr();
    cbreak();
    noecho();
    halfdelay(5);
    refresh();
    curs_set(0);
    keypad(stdscr, TRUE);
    if (has_colors() == FALSE) {
        memset(&scheme, 0, sizeof(scheme));
        tab_vind = 1;
        tab_indF = 1;
    } else {
        start_color();
        initcolorscheme();
    }
    y_status = Y_NONE;
    y_sel = NULL;
    y_sels = 0;
    dualpane = startdp;

    if(argc < 2)
        new_cwd = cwd;
    else
        new_cwd = argv[1];
    layout_win();
    if (!master)
        cleanup_exit(1);
    new_cwd = new_tab_cwd;

    ctx = vcursctx_new(keyhandler_new(0), seltp, 3);
    modes_init();
    ctx->modes[0] = mode_normal;
    ctx->modes[1] = mode_cmd;
    ctx->modes[2] = mode_search;
    vcursctx_mode_switch(ctx, mode_normal);

    int ch = 0;
    while (1) {
        if ((ch = ragetch(1)) != ERR) {
            ctx->v = seltp;
            vcursctx_handle_curses_key(ctx, ch);
        } else {
            int rms = -1;
            int rsl = -1;
            if (master)
                rms = tp_update(master);
            if (slave)
                rsl = tp_update(slave);
            if (cmd_prompt && (rms != -1 || rsl != -1))
                prompt_print(cmd_prompt);
        }
        if (errors) {
            cmd_prompt->visible = 1;
            cmd_prompt->attr = scheme.ERROR_ATTR;
            cmd_prompt->prefix = 0;
            for (size_t i = 0; i < s_errors; i++) {
                prompt_setstr(cmd_prompt, errors[i]);
                prompt_print(cmd_prompt);
                if (ragetch(0) == 27)
                    break;
            }
            prompt_freebuf(cmd_prompt);
            for (size_t i = 0; i < s_errors; i++)
                free(errors[i]);
            s_errors = 0;
            free(errors);
            errors = 0;
            cmd_prompt->prefix = ' ';
            cmd_prompt->attr = scheme.DEFAULT_ATTR;
            cmd_prompt->visible = 0;
            print_all();
        }
    }
    return 0;
}
