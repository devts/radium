typedef struct
{
    int fork;
    int append_all;
    char* file;
    size_t argc;
    char **argv;
} opener;
typedef struct
{
    char *ext;
    opener *o;
} extrule;
int open_file(char *file, dirview *dv);
int open_file_with(char *ex, size_t argc, char **argv, char *file);
