#include "all.h"

static int
strsize(off_t n, char* buf, size_t s_buf)
{
    const char postfixes[] = "BKMGTPE";
    double size;
    int i;

    for (size = n, i = 0; size >= 1024 && i < strlen(postfixes); i++)
        size /= 1024;

    if (!i)
        return snprintf(buf, s_buf, "%ju", (uintmax_t)n);
    else
        return snprintf(buf, s_buf, "%.1f%c", size, postfixes[i]);
}

int
tab_print(tab *t, WINDOW *w)
{
    int mrow, mcol, attr;
    char vind[4], *indF;
    mode_t mode;
    fileinfo *dirs;
    size_t s_left, s_right, dirl, seli, noff, ncur;
    static char *buf = 0;
    static char *spacebuf = 0;
    static size_t s_bufs = 0;

    if(!t || !t->dv || !t->dv->dir)
        return 1;

    mrow = t->mrow;
    mcol = t->mcol;
    if (mcol + 1 > s_bufs) {
        buf = erealloc(buf, mcol + 1);
        spacebuf = erealloc(spacebuf, mcol + 1);
        for (; s_bufs < mcol + 1; s_bufs++)
            spacebuf[s_bufs] = ' ';
    }
    werase(w);
    if (!mrow)
        goto tab_print_cleanup;

    /*write pathbar*/
    wattrset(w, scheme.CWD_ATTR);
    mvwaddnstr(w, 0, 0, t->dv->dir, mcol);

    if (mrow < 2)
        goto tab_print_cleanup;

    if (t->dv->err) {
        wattrset(w, scheme.ERROR_ATTR);
        mvwaddnstr(w, 1, 0, "Error retrieving directory information or content.", mcol);
        goto tab_print_cleanup;
    }

    /*write fileview*/
    if(!t->dv->dirlist) /*empty directory*/
        goto tab_print_cleanup;

    dirl = t->dv->dirlist_size;
    dirs = t->dv->dirlist;
    noff = t->dv->off.n;
    ncur = t->dv->cur.n;
    seli = 0;
    while(seli < t->dv->selection_size && t->dv->selection[seli].n < noff)
        seli++;
    for (size_t i = noff; i < dirl && i < mrow - 2 + noff;i++ ) {
        mode = dirs[i].st.st_mode;
        indF = "";
        if (S_ISLNK(mode)) {
            attr = scheme.LNK_ATTR;
            indF = "@";
        } else if (S_ISDIR(mode)) {
            attr = scheme.DIR_ATTR;
            indF = "/";
        } else if (S_ISBLK(mode) || S_ISCHR(mode)) {
            attr = scheme.DEVICE_ATTR;
        } else if (S_ISFIFO(mode)) {
            attr = scheme.FIFO_ATTR;
            indF = "|";
        } else if (S_ISSOCK(mode)) {
            attr = scheme.SOCKET_ATTR;
            indF = "=";
        } else if (mode & S_ISUID || mode & S_ISGID) {
            attr = scheme.SUID_ATTR;
        } else if (mode & S_IXUSR || mode & S_IXGRP || mode & S_IXOTH) {
            attr = scheme.EXECUTABLE_ATTR;
            indF = "*";
        } else {
            attr = scheme.FILE_ATTR;
        }
        vind[0] = vind[1] = vind[2] = ' ', vind[3] = 0;
        if (seli < t->dv->selection_size && t->dv->selection[seli].n == i) {
            attr = scheme.SELECTED_ATTR;
            vind[2] = '+';
            seli++;
        }
        if (i == ncur) {
            attr |= scheme.CUR_OR_ATTR;
            vind[1] = '>';
        }
        wattrset(w, attr);

        /* fill middle if selected */
        if (i == ncur)
            mvwaddnstr(w, i - noff + 1, 0, spacebuf, mcol);

        /* left side */
        rstrlcpy(buf, tab_vind ? vind : "",  s_bufs);
        rstrlcat(buf, dirs[i].name, mcol + 1);
        rstrlcat(buf, tab_indF ? indF : "", s_bufs);
        mvwaddnstr(w, i - noff + 1, 0, buf, s_bufs);

        /* right side */
        if (S_ISDIR(mode) && dirs[i].csize < 0)
            rstrlcpy(buf, tab_dir_str, s_bufs);
        else
            strsize(S_ISDIR(mode) ? dirs[i].csize : dirs[i].st.st_size, buf, s_bufs);
        if (tab_vind) {
            if (vind[1] == '>')
                vind[1] = '<';
            if (vind[2] == '+')
                vind[2] = ' ', vind[0] = '+';
            rstrlcat(buf, vind, s_bufs);
        }
        mvwaddnstr(w, i - noff + 1, mcol - strlen(buf), buf, s_bufs);
    }

    /*write statusbar*/
    s_left = s_right = 0;
    
    mode = dirs[ncur].st.st_mode;
    char modestr[] = "---------- ";
    if (S_ISREG(mode))
        modestr[0] = '-';
    else if (S_ISBLK(mode))
        modestr[0] = 'b';
    else if (S_ISCHR(mode))
        modestr[0] = 'c';
    else if (S_ISDIR(mode))
        modestr[0] = 'd';
    else if (S_ISFIFO(mode))
        modestr[0] = 'p';
    else if (S_ISLNK(mode))
        modestr[0] = 'l';
    else if (S_ISSOCK(mode))
        modestr[0] = 's';
    else
        modestr[0] = '?';

    if (mode & S_IRUSR) modestr[1] = 'r';
    if (mode & S_IWUSR) modestr[2] = 'w';
    if (mode & S_IXUSR) modestr[3] = 'x';
    if (mode & S_IRGRP) modestr[4] = 'r';
    if (mode & S_IWGRP) modestr[5] = 'w';
    if (mode & S_IXGRP) modestr[6] = 'x';
    if (mode & S_IROTH) modestr[7] = 'r';
    if (mode & S_IWOTH) modestr[8] = 'w';
    if (mode & S_IXOTH) modestr[9] = 'x';

    if (mode & S_ISUID) modestr[3] = (modestr[3] == 'x') ? 's' : 'S';
    if (mode & S_ISGID) modestr[6] = (modestr[6] == 'x') ? 's' : 'S';
    if (mode & S_ISVTX) modestr[9] = (modestr[9] == 'x') ? 't' : 'T';

    wattrset(w, scheme.MODE_ATTR);
    mvwaddnstr(w, mrow - 1, s_left, modestr, sizeof(modestr));
    s_left += sizeof(modestr);

    wattrset(w, scheme.DEFAULT_ATTR);
    struct group *gr;
    struct passwd *pw;
    char name[_SC_LOGIN_NAME_MAX + 2];

    int tmp_len;
    if (!tab_nflag && (pw = getpwuid(dirs[ncur].st.st_uid)))
        tmp_len = snprintf(name, LEN(name) - 1, "%s", pw->pw_name);
    else
        tmp_len = snprintf(name, LEN(name) - 1, "%d", dirs[ncur].st.st_uid);
    tmp_len = MIN(tmp_len, LEN(name) - 2);
    name[tmp_len++] = ' ';
    name[tmp_len] = 0;
    mvwaddnstr(w, mrow - 1, s_left, name, tmp_len);
    s_left += tmp_len;

    if (!tab_nflag && (gr = getgrgid(dirs[ncur].st.st_gid)))
        tmp_len = snprintf(name, LEN(name) - 1, "%s", gr->gr_name);
    else
        tmp_len = snprintf(name, LEN(name) - 1, "%d", dirs[ncur].st.st_gid);
    tmp_len = MIN(tmp_len, LEN(name) - 2);
    name[tmp_len++] = ' ';
    name[tmp_len] = 0;
    mvwaddnstr(w, mrow - 1, s_left, name, tmp_len);
    s_left += tmp_len;

    char mtbuf[20];
    tmp_len = strftime(mtbuf, sizeof(mtbuf), tab_time_fmt, localtime(&(dirs[ncur].st.st_mtime)));
    name[tmp_len] = ' ';
    tmp_len++;
    mvwaddnstr(w, mrow - 1, s_left, mtbuf, tmp_len);
    s_left += tmp_len;

    if(S_ISLNK(mode))
    {
        char lp[PATH_MAX + 4];
        lp[0] = '-'; lp[1] = '>'; lp[2] = ' ';
        char p[PATH_MAX];
        if (rstrlcpy(p, t->dv->dir, sizeof(p)) || rstrlcat(p, dirs[ncur].name, sizeof(p)))
            tmp_len = -1;
        else
            tmp_len = readlink(p, lp + 3, sizeof(lp)-4);
        if(tmp_len == -1)
        {
            lp[3] = '?'; lp[4] = 0;
            tmp_len = 1;
        }
        tmp_len += 3;
        mvwaddnstr(w, mrow - 1, s_left, lp, tmp_len);
        s_left += tmp_len;
    }

    s_right = snprintf(mtbuf, sizeof(mtbuf), "%zu/%zu", ncur + 1, dirl);
    mvwaddnstr(w, mrow - 1, mcol - s_right, mtbuf, s_right);

tab_print_cleanup:
    wrefresh(w);
    return 0;
}

static dirview *
tab_find_histentry(tab *t, char *cwd)
{
    for(size_t i = 0; i < t->dv_hist_size; i++) 
        if (!strcmp((t->dv_hist[i])->dir, cwd)) 
            return t->dv_hist[i];
    return NULL;
}

static int
tab_change_dir_hl(tab *t, char* dir, int update_hl)
{
    dirview *new;
    if (!t)
        return 1;
    if (t->dv && !strcmp(t->dv->dir, dir))
        return dv_update(t->dv, 0) > 0;
    if ((new = tab_find_histentry(t, dir))) {
        dv_update(new, 0);
    } else {
        if (!(new = dv_new(dir)))
            return 1;
        t->dv_hist = ereallocarray(t->dv_hist, ++(t->dv_hist_size), sizeof(dirview*));
        t->dv_hist[t->dv_hist_size - 1] = new;
    }

    if(!new)
        return 1;

    size_t l = strlen(dir);
    if(t->dv && strlen(t->dv->dir) > l && !strncasecmp(t->dv->dir, dir, l))
    {
        char *newselbuf = estrdup(t->dv->dir);
        char *newsel = newselbuf + l;
        char *end = strchr(newsel, '/');
        if (end)
            *end = 0;
        dv_update_cur(new, newsel);
        free(newselbuf);
    }

    if(t->dv)
        dv_free_dirlist(t->dv);
    t->dv = new;

    if (update_hl) {
        if (!t->hl_hist_size || t->hl_pos == t->hl_hist_size - 1) {
            t->hl_hist = ereallocarray(t->hl_hist, ++(t->hl_hist_size), sizeof(char *));
            t->hl_pos = t->hl_hist_size - 1;
            t->hl_hist[t->hl_pos] = estrdup(new->dir);
        } else {
            for (size_t i = ++(t->hl_pos); i < t->hl_hist_size; i++)
                free(t->hl_hist[i]);
            t->hl_hist = ereallocarray(t->hl_hist, (t->hl_hist_size = t->hl_pos + 1), sizeof(char *));
            t->hl_hist[t->hl_pos] = estrdup(new->dir);
        }
    }
    return 0;
}

int
tab_H_cd(tab *t, size_t s)
{
    if (t->hl_pos < s)
        return 1;
    t->hl_pos -= s;
    return tab_change_dir_hl(t, t->hl_hist[t->hl_pos], 0);
}

int
tab_L_cd(tab *t, size_t s)
{
    if (t->hl_pos + s >= t->hl_hist_size)
        return 1;
    t->hl_pos += s;
    return tab_change_dir_hl(t, t->hl_hist[t->hl_pos], 0);
}

static int
tab_change_dir(tab *t, char* dir)
{
    return tab_change_dir_hl(t, dir, 1);
}

int
tab_cd_rel(tab *t, char *wd, char* arg)
{
    char *dir;
    int r;
    if (!t)
        return 1;
    if (!(dir = canonic_path(wd, arg)))
        return 1;
    r = tab_change_dir(t, dir);
    free(dir);
    return r;
}

int
tab_cd(tab *t, char* arg)
{
    if(!t || !t->dv)
        return 1;
    return tab_cd_rel(t, t->dv->dir, arg);
}

void
tab_bring_cur_in_view(tab *t)
{
    size_t mrow = t->mrow;
    if (mrow < 3 || !t->dv->dirlist_size)
        return;
    mrow -= 2;
	size_t eso = MIN((mrow - 1) / 2, so);
    size_t ws = mrow - 2*eso;
    if (!ws)
        return;
    size_t off = t->dv->off.n + eso;
    if (t->dv->cur.n < off)
        off = t->dv->cur.n;
    if (t->dv->cur.n > off + ws - 1)
        off = t->dv->cur.n + 1 - ws;
    if (off + eso + ws >= t->dv->dirlist_size)
        off = t->dv->dirlist_size > ws + eso ? t->dv->dirlist_size - ws - eso : 0;
    dv_update_noff(t->dv, off > eso ? off - eso : 0);
}
int
tab_move_to(tab *t, size_t n)
{
    if(!t)
        return 1;
    if(dv_update_ncur(t->dv, n))
        return 1;
    return 0;
}
int
tab_move_rel(tab *t, int n)
{
    if(!t)
        return 1;
    return tab_move_to(t, t->dv->cur.n + n);
}
void
tab_move_top(tab *t)
{
    size_t mrow = t->mrow;
    if (mrow < 3 || !t->dv->dirlist_size)
        return;
    mrow -= 2;
	size_t eso = MIN((mrow - 1) / 2, so);
    if (!t->dv->off.n)
        tab_move_to(t, 0);
    else
        tab_move_to(t, t->dv->off.n + eso);
}
void
tab_move_middle(tab *t)
{
    size_t mrow = t->mrow;
    if (mrow < 3 || !t->dv->dirlist_size)
        return;
    mrow -= 2;
    tab_move_to(t, t->dv->off.n + MIN((t->dv->dirlist_size - 1)/2, (mrow - 1)/2));
}
void
tab_move_bottom(tab *t)
{
    size_t mrow = t->mrow;
    if (mrow < 3 || !t->dv->dirlist_size)
        return;
    mrow -= 2;
	size_t eso = MIN((mrow - 1) / 2, so);
    if (t->dv->dirlist_size <= t->dv->off.n + mrow)
        tab_move_to(t, t->dv->dirlist_size - 1);
    else
        tab_move_to(t, t->dv->off.n + mrow - 1 - eso);
}
void
tab_page_up(tab *t, size_t n, size_t m, size_t sl)
{
    size_t mrow = t->mrow;
    if (mrow < 3 || !t->dv->dirlist_size)
        return;
    mrow -= 2;
    if (m)
        sl = mrow * n / m;
    if (t->dv->cur.n > sl) {
        dv_update_ncur(t->dv, t->dv->cur.n - sl);
        dv_update_noff(t->dv, t->dv->off.n > sl ? t->dv->off.n - sl : 0);
    } else {
        dv_update_noff(t->dv, 0);
        dv_update_ncur(t->dv, 0);
    }
}
void
tab_page_down(tab *t, size_t n, size_t m, size_t sl)
{
    size_t mrow = t->mrow;
    if (mrow < 3 || !t->dv->dirlist_size)
        return;
    mrow -= 2;
    if (m)
        sl = mrow * n / m;
    if (t->dv->cur.n + sl < t->dv->dirlist_size) {
        dv_update_ncur(t->dv, t->dv->cur.n + sl);
        dv_update_noff(t->dv, t->dv->off.n + sl < t->dv->dirlist_size ?
                t->dv->off.n + sl : t->dv->dirlist_size - 1);
    } else {
        dv_update_noff(t->dv, t->dv->dirlist_size - 1);
        dv_update_ncur(t->dv, t->dv->dirlist_size - 1);
    }
    tab_bring_cur_in_view(t);
}
void
tab_cur_to_middle(tab *t)
{
    size_t mrow = t->mrow;
    if (mrow < 3 || !t->dv->dirlist_size)
        return;
    mrow -= 2;
    if (t->dv->cur.n > (mrow - 1) / 2)
        dv_update_noff(t->dv, t->dv->cur.n - (mrow - 1) / 2);
    else
        dv_update_noff(t->dv, 0);
}
void
tab_cur_to_top(tab *t)
{
    if (!t->dv->dirlist_size)
        return;
    dv_update_noff(t->dv, t->dv->dirlist_size - 1);
    tab_bring_cur_in_view(t);
}
void
tab_cur_to_bottom(tab *t)
{
    if (!t->dv->dirlist_size)
        return;
    dv_update_noff(t->dv, 0);
    tab_bring_cur_in_view(t);
}
int
tab_enter_cur(tab *t)
{
    if(!t || !t->dv->dirlist)
        return 1;
    if(S_ISDIR(t->dv->dirlist[t->dv->cur.n].st_t.st_mode))
        return tab_cd(t, t->dv->cur.name);
    if(chdir(t->dv->dir))
        return 1;
    return open_file(t->dv->cur.name, t->dv);
}
int
tab_del(tab* t)
{
    size_t i;
    if (!t)
        return 1;
    for (i = 0; i < t->dv_hist_size; i++)
        dv_del(t->dv_hist[i]);
    free(t->dv_hist);
    for (i = 0; i < t->hl_hist_size; i++)
        free(t->hl_hist[i]);
    free(t->hl_hist);
    free(t);
    return 0;
}

tab *
tab_new(char *cwd, char *arg)
{
    tab *t = ecalloc(1, sizeof(tab)); 
    if (tab_cd_rel(t, cwd, arg) > 0)
    {
        tab_del(t);
        return NULL;
    }
    return t;
}

int
tab_search_next(tab *t, char *reg)
{
    if(!t || !t->dv)
        return 1;
    size_t nr;
    int res = dv_search_next(t->dv, reg, &nr);
    if(!res)
        tab_move_to(t, nr);
    return res;
}
int
tab_search_prev(tab *t, char *reg)
{
    if(!t || !t->dv)
        return 1;
    size_t nr;
    int res = dv_search_prev(t->dv, reg, &nr);
    if(!res)
        tab_move_to(t, nr);
    return res;
}
