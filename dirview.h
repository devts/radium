typedef struct selitem selitem;
struct selitem
{
    size_t n;
    char* name;
};

typedef struct dirview dirview;
struct dirview
{
    char* dir;
    fileinfo *dirlist;
    size_t dirlist_size;
    int err; /* 0 = dirlist valid, 1 = error retrieving dirlist, 2 = dirlist deleted or not retrieved yet */
    sort st;

    selitem off;
    selitem cur;

    selitem *selection;
    size_t selection_size;

    struct timespec dir_mtim;
};

dirview *dv_new(char* dir);
void dv_del(dirview *dv);
int dv_update(dirview *dv, int force);
void dv_free_dirlist(dirview *dv);

/*1 = error. On Error values are reset to nsel=noff=0
 * and sel and off are set to consitent values
 * if dirlist_size > 0 and to NULL otherwise*/
int dv_update_cur(dirview *dv, char* cur);
int dv_update_off(dirview *dv, char* off);
int dv_update_ncur(dirview *dv, size_t ncur);
int dv_update_noff(dirview *dv, size_t noff);
int dv_update_selitem_name(dirview *dv, selitem *sel, char* name, int resel);
int dv_update_selitem_n(dirview *dv, selitem *sel, size_t nsel);
void dv_sort(dirview *dv, sort s);

int dv_select_item(dirview *dv, size_t n);
int dv_unselect_item(dirview *dv, size_t n);
int dv_toggleselect_item(dirview *dv, size_t n);
int dv_empty_selection(dirview *dv);
int dv_regsel(dirview *dv, char *reg);
int dv_iterate_as(dirview *dv, int (*func)(char *path, fileinfo *fi, void *v), void *v);

int dv_search_next(dirview *dv, char *reg, size_t *nr);
int dv_search_prev(dirview *dv, char *reg, size_t *pr);
